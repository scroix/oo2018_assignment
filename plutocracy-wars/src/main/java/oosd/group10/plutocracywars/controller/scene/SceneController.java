package oosd.group10.plutocracywars.controller.scene;

import javafx.scene.image.Image;
import javafx.scene.layout.*;

public class SceneController {
    private Pane rootPane;

    public static final int SCENE_WIDTH = 512;
    public static final int SCENE_HEIGHT = 605;

    public SceneController() {
        // Create Root Pane
        rootPane = new Pane();
    }
    
    public Pane getRootPane() {
    		return rootPane;
    }

    public void drawUI() {
        // Draw background and set Pane size.
        BackgroundSize backgroundSize = new BackgroundSize(SCENE_WIDTH, SCENE_HEIGHT, true, true, true, false);
        BackgroundImage myBI= new BackgroundImage(new Image("Background.png",SCENE_WIDTH,SCENE_HEIGHT,true,true),
                BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.CENTER,
                backgroundSize);

        rootPane.setPrefSize(SCENE_WIDTH, SCENE_HEIGHT);
        rootPane.setBackground(new Background(myBI));
    }
}
