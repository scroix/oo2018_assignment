package oosd.group10.plutocracywars.controller.scene;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;
import oosd.group10.plutocracywars.controller.GameSceneController;
import oosd.group10.plutocracywars.view.PWButton;
import oosd.group10.plutocracywars.view.menu.MenuView;

public class MainMenuSceneController extends SceneController {
    public MainMenuSceneController() {
    	super();
    	
        drawUI();
    }
    
    @Override
    public void drawUI() {
        // Call super class
        super.drawUI();
        
        // Draw scene background
        BackgroundSize backgroundSize = new BackgroundSize(SCENE_WIDTH-30, 120, true, true, true, false);
        BackgroundImage myBI= new BackgroundImage(new Image("Title.png",SCENE_WIDTH-30,120,true,true),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
                backgroundSize);
        Pane logoPane = new Pane();
        logoPane.setPrefSize(SCENE_WIDTH-30, 120);
        logoPane.setBackground(new Background(myBI));
        logoPane.setLayoutX(15);
        logoPane.setLayoutY(5);
        
        // Draw menu view and add to root pane
        MenuView view = new MenuView();
        view.setPrefSize(SCENE_WIDTH, SCENE_HEIGHT); 
        view.setLayoutX(0);
        view.setLayoutY(0);
        view.startButton.setOnMousePressed(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                GameSceneController.getInstance().ChangeGameScene(GameSceneController.GameScene.Lobby);
            }
        });
        getRootPane().getChildren().addAll(view,logoPane);
    }
}
