package oosd.group10.plutocracywars.controller;

import javafx.scene.Scene;
import oosd.group10.plutocracywars.controller.scene.BattleSceneController;
import oosd.group10.plutocracywars.controller.scene.LobbySceneController;
import oosd.group10.plutocracywars.controller.scene.MainMenuSceneController;
import oosd.group10.plutocracywars.controller.scene.SceneController;
import oosd.group10.plutocracywars.model.game.Game;
import oosd.group10.plutocracywars.model.game.GameImpl;

public class GameSceneController {
    // Instance
    private static GameSceneController instance;
    public static GameSceneController getInstance() {
        if (instance == null) {
            instance = new GameSceneController();
        }
        return instance;
    }

    // Enums
    public enum GameScene {
        None,
        MainMenu,
        Lobby,
        Battle
    }

    // Properties
    private Scene scene;
    public void setScene(Scene theScene) {
        scene = theScene;
    }

    public GameScene currentScene = GameScene.None;
    
    public boolean ChangeGameScene(GameScene gameScene) {
    	return ChangeGameScene(gameScene, null);
    }
    public boolean ChangeGameScene(GameScene gameScene,Object argument) {
        currentScene = gameScene;
        SceneController controller = null;
        switch (gameScene) {
            case MainMenu: {
                controller = new MainMenuSceneController();
                break;
            }
            case Lobby: {
                controller = new LobbySceneController();
                break;
            }
            case Battle: {
                controller = new BattleSceneController((Game)argument);
                break;
            }
        }
        
        if (controller == null) 
        	return false;
        
        scene.setRoot(controller.getRootPane());
        return true;
    }
}
