package oosd.group10.plutocracywars.controller.scene;

import java.util.Set;

import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;
import oosd.group10.plutocracywars.BattleInitaliser;
import oosd.group10.plutocracywars.model.board.BoardCell;
import oosd.group10.plutocracywars.model.board.piece.BoardPiece;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnit;
import oosd.group10.plutocracywars.model.board.piece.unit.attack.Attacker;
import oosd.group10.plutocracywars.model.board.piece.unit.attack.AbstractAttackingUnit;
import oosd.group10.plutocracywars.model.game.Game;
import oosd.group10.plutocracywars.model.game.TurnObserver;
import oosd.group10.plutocracywars.view.battle.BoardView;
import oosd.group10.plutocracywars.view.battle.CellView;
import oosd.group10.plutocracywars.view.battle.HUDView;

public class BattleSceneController extends SceneController implements TurnObserver {
	
	public static final int BOARD_WIDTH = SCENE_WIDTH - 100;
	
    private BoardView boardView;
    private HUDView hudView;
	
	private Game game;
	
	private int columns;
	private int rows;
		
    private PlayerUnit currentlySelectedUnit;
    private BoardCell currentlySelectedCell;
    
    private boolean recentlyTriggeredTurn;

    /*
     * Manage interaction between user's view and board model.
     */
	public BattleSceneController(Game game) {
		// Create our game MODEL!
		this.game = game;
		
		// ...and setup CONTROLLERS
		BattleInitaliser setup = new BattleInitaliser(game);
		game.attachTurnObserver(this);
		
		columns = game.getBoard().getColumns();
		rows = game.getBoard().getRows();
				
		// Draw UI
		drawUI();

		addBoardCellHandler();
		addResetBoardHandler();
	}
	
    @Override
    public void drawUI() {
        super.drawUI();

        // the view is divided into two parts, the board and the hud
        drawBoard();
        drawHUD();
        
        getRootPane().getChildren().addAll(boardView,hudView);

		drawBoardPieces();
		highlightCurrentPlayer();
    }
    
    protected void drawBoard() {
    	
        boardView = new BoardView(game.getBoard(),BOARD_WIDTH,BOARD_WIDTH,columns,rows);
        
        int boardInset = (SCENE_WIDTH-BOARD_WIDTH)/2;
        boardView.resize(BOARD_WIDTH,BOARD_WIDTH);
        boardView.setLayoutX(boardInset);
        boardView.setLayoutY(boardInset);
        
        BackgroundSize backgroundSize = new BackgroundSize(SCENE_WIDTH, SCENE_HEIGHT, true, true, true, false);
        BackgroundImage myBI= new BackgroundImage(new Image("BattleBackground.png",SCENE_WIDTH,SCENE_HEIGHT,true,true),
                BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.CENTER,
                backgroundSize);

        Pane battleBackgroundPane = new Pane();
        battleBackgroundPane.setPrefSize(SCENE_WIDTH, SCENE_HEIGHT);
        battleBackgroundPane.setBackground(new Background(myBI));
        getRootPane().getChildren().add(battleBackgroundPane);
    }
    
    protected void drawHUD() {
        hudView = new HUDView();
        hudView.setPrefSize(SCENE_WIDTH,92);
        hudView.setLayoutX(0);
        hudView.setLayoutY(SCENE_HEIGHT-92);
    }
    
    protected void drawBoardPieces() {
    	
    	boardView.clearPieces();
    	
    	for (int y = 0; y < rows; y++) {
			for (int x = 0; x < columns; x++) {
				BoardCell boardCell = game.getBoard().getBoardCellByXAndYCoordinates(x,y);
				if (boardCell.isOccupied()) {
					boardView.drawPiece(boardCell.getOccupant());
				}
			}
		}
    	
    	drawShieldedPieces();
    }
    
    private void drawShieldedPieces() {
    	
    	for (PlayerUnit unit : game.getAllPlayerUnits()) {
    		if (unit instanceof Attacker) {
				if (((Attacker) unit).getShieldStatus() && !game.getCurrentPlayer().getUnits().contains(unit)) {
					boardView.DrawShield(unit);
				}
				else {
					boardView.RemoveShield(unit);
				}
    		}
    	}
    }
    
    private void removeOldShields() {
    	
    	for (PlayerUnit unit : game.getAllPlayerUnits()) {
    		if (unit instanceof Attacker) {
				if (((Attacker) unit).getShieldStatus() && !game.getCurrentPlayer().getUnits().contains(unit)) {
					((Attacker) unit).setShieldStatus(true, true);
				}
				else {
					((Attacker) unit).setShieldStatus(false, true);
				}
    		}
    	}
    }
    
    private void addResetBoardHandler() {
    	
    	hudView.getResetButton().setOnMouseClicked(new EventHandler<MouseEvent>() {
    		public void handle(MouseEvent event) {
    			
    			if (game.canResetTurn(game.getCurrentPlayer())) {
    				game.resetTurn(game.getCurrentPlayer(), hudView.getResetCount());
    				updateViewOnStateOfBoard();
    			}
    		}
    	});
    }
    
    /*
	 * Primary logic for cell selection, including matching with model, moving and highlighting.
	 */
	private void addBoardCellHandler() {
		boardView.getParent().setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {

				recentlyTriggeredTurn = false;
				BoardCell justSelectedCell = getBoardCellFromMouseLocation(event.getTarget());
				
				// Had we already selected a unit before this event occurred?
				boolean unitCanAttack = game.getCurrentPlayer().getUnitsAbleToAttack().contains(currentlySelectedUnit);
				boolean unitCanMove = game.getCurrentPlayer().getUnitsAbleToMove().contains(currentlySelectedUnit);
				
				if (currentlySelectedUnit != null && justSelectedCell != null) {
	
					if (unitCanMove) {
						if (currentlySelectedUnit.canMoveTo(justSelectedCell)) {
								currentlySelectedUnit.moveTo(justSelectedCell);
						}	
					}
					else if (unitCanAttack) {
						if (((Attacker) currentlySelectedUnit).canAttack(justSelectedCell) && !isShielded(justSelectedCell)) {
							((Attacker) currentlySelectedUnit).attack(justSelectedCell);
						}
						else {
							((Attacker) currentlySelectedUnit).setShieldStatus(true, false);
						}
					}
				}
				
				// if we literally just finished a turn, you can skip this part...
				if (!recentlyTriggeredTurn) {
					currentlySelectedCell = justSelectedCell;
					currentlySelectedUnit = getUnitFromBoardCell(currentlySelectedCell);
				}
				
				updateViewOnStateOfBoard();
				
				if (game.isFinished()) {
					System.out.println(game.getWinner());
				}
			}	
		});
	}
		
	@Override
	public void nextTurnOccurred() {
		
		recentlyTriggeredTurn = true;
		
		currentlySelectedUnit = null;
		currentlySelectedCell = null;
		
		highlightCurrentPlayer();
		removeOldShields();
	}

	private void updateViewOnStateOfBoard() {
		drawBoardPieces();
		resetAllCellHighlights();
		highlightCurrentlySelectedCell();
		
		if (currentlySelectedUnit != null)
			if (game.getCurrentPlayer().getUnits().contains(currentlySelectedUnit))
				return;
		
		highlightCurrentPlayer();
	}
	
	/*
	 * Highlights a selected cell and any associated movable cells.
	 */
	private void highlightCurrentlySelectedCell() {
	
		if (currentlySelectedCell != null) {
			
			// Update the HUD.
			hudView.updateSelectedUnit(currentlySelectedCell.getOccupant());
			hudView.setResetVisbility(false);

			// Alter the cell's visible state
			CellView cellView = boardView.boardCells[currentlySelectedCell.getX()][currentlySelectedCell.getY()];
			cellView.SetState(CellView.CellViewState.Selected);

			// Alter the cell's potential movable cells
			if (currentlySelectedUnit != null) {
				if (game.getCurrentPlayer().getUnits().contains(currentlySelectedUnit)) {
					
					highlightUnitMovableCells(currentlySelectedUnit);
					
					// Alter the cell's potential attackable cells
					if (game.getCurrentPlayer().getUnitsAbleToAttack().contains(currentlySelectedUnit)) {
						if (currentlySelectedUnit.hasMoved()) {
							highlightUnitHostileCells((Attacker) currentlySelectedUnit);
						}
					}
				}
			}
		}	
	}
	
	/*
	 * Show the unit's movable cells
	 */
	private void highlightUnitMovableCells(PlayerUnit unit) {

		Set<BoardCell> moveableCells = unit.getBoardCellsCanMoveTo();
		
		for (BoardCell cell : moveableCells) {
			CellView moveCellView = boardView.boardCells[cell.getX()][cell.getY()];
			moveCellView.SetState(CellView.CellViewState.MovementHighlight);
		}
	}
	
	/*
	 * Show the unit's attackable cells
	 */
	private void highlightUnitHostileCells(Attacker attacker) {
				
		for (BoardCell cellInRangeOfAttack : (((AbstractAttackingUnit) attacker).getAttackPattern().getCellsInPattern(((BoardPiece) attacker).getLocation()))) {
			CellView cell = boardView.boardCells[(int) cellInRangeOfAttack.getX()][(int) cellInRangeOfAttack.getY()];
			
			// There's a unit in the cell
			if (cellInRangeOfAttack.isOccupied()) {
				
				// The unit belongs to the current player, hide it.
				if (isFriendlyFire(getUnitFromBoardCell(cellInRangeOfAttack))) {
					cell.SetState(CellView.CellViewState.None);
				}
				else if (isShielded(cellInRangeOfAttack)) {
					cell.SetState(CellView.CellViewState.ShieldHighlight);
				}
				// The unit belongs to the enemy, and isn't immune to the technology type
				else if (cellInRangeOfAttack.getOccupant().isDefeatedBy(((BoardPiece) attacker).getTechnologyType())) {
					cell.SetState(CellView.CellViewState.AttackHighlight);
				}
				// The unit belongs to the enemy and is immune to the technology type
				else {
					cell.SetState(CellView.CellViewState.ImmuneHighlight);
				}
			}
			// There's nothing in the cell, but we should show it to indicate where this unit could attack
			else {
				cell.SetState(CellView.CellViewState.TargetHighlight);
			}
		}
	}

	/*
	 * Highlight current players units 
	 */
	private void highlightCurrentPlayer() {
		
		hudView.setUnitText(game.getCurrentPlayer().getName());
		hudView.setPlayerText("It's your move");
		hudView.hideProfileImage();
		
		hudView.setResetVisbility(true);
		
		for (PlayerUnit unit : game.getCurrentPlayer().getUnits()) {
			CellView moveCellView = boardView.boardCells[unit.getLocation().getX()][unit.getLocation().getY()];
			if (!unit.hasMoved())
				moveCellView.SetState(CellView.CellViewState.TurnHighlight);
		}
	}

	private void resetAllCellHighlights() {
		for (int column = 0; column < columns; column++) {
			for (int row = 0; row < rows; row++) {
				boardView.boardCells[column][row].SetState(CellView.CellViewState.None);
			}
		}
	}

	/*
	 * Iterate through all the board model cells until we find one which matches the
	 * view's target.
	 */
	private BoardCell getBoardCellFromMouseLocation(EventTarget eventTarget) {
		
		for (int column = 0; column < columns; column++) {
			for (int row = 0; row < rows; row++) {
	
				if (boardView.boardCells[row][column].equals(eventTarget)) {
					return game.getBoard().getBoardCellByXAndYCoordinates(row, column);
				}
			}
		}
		return null;
	}

	/*
	 * A simple check which provides a player unit, if contained within the cell.
	 */
	private PlayerUnit getUnitFromBoardCell(BoardCell cell) {
		
		if (cell != null) {
			BoardPiece occupant = cell.getOccupant();
			if (game.getAllPlayerUnits().contains(occupant)) {
				return (PlayerUnit) occupant;
			}
		}
		
		return null;
	}

	protected boolean isUnitOfCurrentPlayer(PlayerUnit unit) {
		
		if (game.getCurrentPlayer().getUnits().contains(unit))
			return true;
		
		return false;
	}

	private boolean isShielded(BoardCell cell) {
		
		PlayerUnit unit = getUnitFromBoardCell(cell);
		
		if (unit != null) {
			
			if (unit instanceof Attacker) {
				
				return ((Attacker) unit).getShieldStatus();
			}
		}
		
		return false;
	}

	private boolean isFriendlyFire(PlayerUnit unit) {
		if (game.getCurrentPlayer().getUnits().contains(unit))
			if (!unit.equals(currentlySelectedUnit))
				return true;
		return false;
	}

}
	