package oosd.group10.plutocracywars.controller.scene;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import oosd.group10.plutocracywars.BattleInitaliser;
import oosd.group10.plutocracywars.controller.GameSceneController;
import oosd.group10.plutocracywars.model.board.BoardImpl;
import oosd.group10.plutocracywars.model.game.Game;
import oosd.group10.plutocracywars.model.game.GameImpl;
import oosd.group10.plutocracywars.view.PWTextField;
import oosd.group10.plutocracywars.view.lobby.LobbyView;
import oosd.group10.plutocracywars.view.menu.MenuView;

public class LobbySceneController extends SceneController  {
	private LobbyView lobbyView;
	
	public LobbySceneController() {
		super();
		
        // Draw UI
        drawUI();
    }
	
	@Override
    public void drawUI() {
        // Call super class
        super.drawUI();
        
        BackgroundSize logoBackgroundSize = new BackgroundSize(SCENE_WIDTH-30, 120, true, true, true, false);
        BackgroundImage logoBI= new BackgroundImage(new Image("Title.png",SCENE_WIDTH-30,120,true,true),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
                logoBackgroundSize);
        Pane logoBackgroundPane = new Pane();
        logoBackgroundPane.setPrefSize(SCENE_WIDTH-30, 120);
        logoBackgroundPane.setBackground(new Background(logoBI));
        logoBackgroundPane.setLayoutX(15);
        logoBackgroundPane.setLayoutY(5);
        
        BackgroundSize backgroundSize = new BackgroundSize(SCENE_WIDTH, 485, true, true, true, false);
        BackgroundImage myBI= new BackgroundImage(new Image("LobbyBackground.png",SCENE_WIDTH,485,true,true),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
                backgroundSize);
        
        Pane backgroundPane = new Pane();
        backgroundPane.setPrefSize(SCENE_WIDTH, 485);
        backgroundPane.setLayoutY(SCENE_HEIGHT-485);
        backgroundPane.setBackground(new Background(myBI));
        
        // Draw lobby view and add to root pane
        lobbyView = new LobbyView();
        lobbyView.setPrefSize(SCENE_WIDTH, SCENE_HEIGHT); 
        lobbyView.setLayoutX(0);
        lobbyView.setLayoutY(0);
        lobbyView.startButton.setOnMousePressed(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
            	SetupGame();
            }
        });
        getRootPane().getChildren().addAll(logoBackgroundPane,backgroundPane,lobbyView);
    }
	
	public void SetupGame() {
		if (lobbyView.playerOneNameTF.getText().equals("") && lobbyView.playerTwoNameTF.getText().equals("")) {
			return;
		}
		
		int roundTime = Integer.parseInt(lobbyView.timeLimitTF.getText());
		
		GameImpl gameImpl = new GameImpl(lobbyView.playerOneNameTF.getText(),lobbyView.playerTwoNameTF.getText(),lobbyView.playerUnitCountToggle.getCurrentValue(),roundTime);
		gameImpl.setBoard(new BoardImpl(lobbyView.boardSizeToggle.getCurrentValue(),lobbyView.boardSizeToggle.getCurrentValue()));
		
		BattleInitaliser setup = new BattleInitaliser(gameImpl);
		
		GameSceneController.getInstance().ChangeGameScene(GameSceneController.GameScene.Battle,gameImpl);
	}
}
