package oosd.group10.plutocracywars.view.battle;

public class ImageableImpl implements Imageable {
	
	private String imageName;
	private String profileImageName;

	public ImageableImpl(String imageName, String profileImageName) {
		this.imageName = imageName;
		this.profileImageName = profileImageName;
	}

	@Override
	public String getImageName() {
		return imageName;
	}

	@Override
	public String getProfileImageName() {
		return profileImageName;
	}

}
