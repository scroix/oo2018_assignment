package oosd.group10.plutocracywars.view.battle;

import static oosd.group10.plutocracywars.model.board.piece.TechnologyType.DARK_MATTER;
import static oosd.group10.plutocracywars.model.board.piece.TechnologyType.EXPLOSIVE;
import static oosd.group10.plutocracywars.model.board.piece.TechnologyType.GRAVITY;
import static oosd.group10.plutocracywars.model.board.piece.TechnologyType.NANOTECH;
import static oosd.group10.plutocracywars.model.board.piece.TechnologyType.PLASMA;
import static oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitType.BESERKER;
import static oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitType.CHARGER;
import static oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitType.JUGGERNAUT;
import static oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitType.RIFLEMAN;
import static oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitType.ROCKETEER;
import static oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitType.SPECOPS;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import oosd.group10.plutocracywars.model.board.BoardCell;
import oosd.group10.plutocracywars.model.board.piece.BoardPiece;
import oosd.group10.plutocracywars.model.board.piece.Obstacle;
import oosd.group10.plutocracywars.model.board.piece.TechnologyType;
import oosd.group10.plutocracywars.model.board.piece.Tribble;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnit;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitType;

public class ImageableBoardPieceImpl implements ImageableBoardPiece {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4465105472724343517L;
	private static final Map<PlayerUnitType, Imageable> PLAYER_UNIT_IMAGEABLES;
	private static final Map<TechnologyType, Imageable> OBSTACLE_IMAGEABLES;

	private BoardPiece boardPiece;

	public ImageableBoardPieceImpl(BoardPiece boardPiece) {
		this.boardPiece = boardPiece;
	}

	@Override
	public String getImageName() {
		if (boardPiece instanceof PlayerUnit) {
			PlayerUnit playerUnit = (PlayerUnit) boardPiece;
			return getImageableFromPlayerUnit(playerUnit).getImageName();
		} else if (boardPiece instanceof Obstacle) {
			Obstacle obstacle = (Obstacle) boardPiece;
			return getImageableFromObstacle(obstacle).getImageName();
		} else {
			throw new IllegalStateException(
					"Cannot get image name because BoardPiece class is unknown: " + boardPiece.getClass());
		}
	}

	@Override
	public String getProfileImageName() {
		if (boardPiece instanceof PlayerUnit) {
			PlayerUnit playerUnit = (PlayerUnit) boardPiece;
			return getImageableFromPlayerUnit(playerUnit).getProfileImageName();
		} else if (boardPiece instanceof Obstacle) {
			Obstacle obstacle = (Obstacle) boardPiece;
			return getImageableFromObstacle(obstacle).getProfileImageName();
		} else {
			throw new IllegalStateException(
					"Cannot get profile image name because BoardPiece class is unknown: " + boardPiece.getClass());
		}
	}

	@Override
	public BoardCell getLocation() {
		return boardPiece.getLocation();
	}

	@Override
	public TechnologyType getTechnologyType() {
		return boardPiece.getTechnologyType();
	}

	@Override
	public boolean isDefeatedBy(TechnologyType technologyType) {
		return boardPiece.isDefeatedBy(technologyType);
	}

	@Override
	public Set<TechnologyType> getDefeatedBy() {
		return boardPiece.getDefeatedBy();
	}

	@Override
	public void kill() {
		boardPiece.kill();
	}

	private Imageable getImageableFromPlayerUnit(PlayerUnit playerUnit) {
		return PLAYER_UNIT_IMAGEABLES.get(playerUnit.getPlayerUnitType());
	}

	private Imageable getImageableFromObstacle(Obstacle obstacle) {
		if (obstacle instanceof Tribble) {
	        return new ImageableImpl("Tribble.png", "Tribble.png"); // Or reference to static instance
	    } else {
	        return OBSTACLE_IMAGEABLES.get(obstacle.getTechnologyType());
	    }
	}

	//@formatter:off
	static {
		PLAYER_UNIT_IMAGEABLES = new HashMap<>();
		PLAYER_UNIT_IMAGEABLES.put(RIFLEMAN, new ImageableImpl("Rifleman.png", "RiflemanProfile.png"));
		PLAYER_UNIT_IMAGEABLES.put(BESERKER, new ImageableImpl("Beserker.png", "BeserkerProfile.png"));
		PLAYER_UNIT_IMAGEABLES.put(ROCKETEER, new ImageableImpl("Rocketeer.png", "RocketeerProfile.png"));
		PLAYER_UNIT_IMAGEABLES.put(CHARGER, new ImageableImpl("Charger.png", "ChargerProfile.png"));
		PLAYER_UNIT_IMAGEABLES.put(JUGGERNAUT, new ImageableImpl("Juggernaut.png", "JuggernautProfile.png"));
		PLAYER_UNIT_IMAGEABLES.put(SPECOPS, new ImageableImpl("SpecOps.png", "SpecOpsProfile.png"));

		OBSTACLE_IMAGEABLES = new HashMap<>();
		OBSTACLE_IMAGEABLES.put(DARK_MATTER, new ImageableImpl("DarkMatterObstacle.png", "DarkMatterObstacle.png"));
		OBSTACLE_IMAGEABLES.put(EXPLOSIVE, new ImageableImpl("ExplosiveObstacle.png", "ExplosiveObstacle.png"));
		OBSTACLE_IMAGEABLES.put(GRAVITY, new ImageableImpl("GravityObstacle.png", "GravityObstacle.png"));
		OBSTACLE_IMAGEABLES.put(NANOTECH, new ImageableImpl("NanoTechObstacle.png", "NanoTechObstacle.png"));
		OBSTACLE_IMAGEABLES.put(PLASMA, new ImageableImpl("PlasmaObstacle.png", "PlasmaObstacle.png"));
	}
	//@formatter:on

}
