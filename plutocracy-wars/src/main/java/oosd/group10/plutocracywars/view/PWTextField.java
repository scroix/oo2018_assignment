package oosd.group10.plutocracywars.view;
import javafx.geometry.Pos;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;

public class PWTextField extends TextField {
	private String backgroundImage;
	public PWTextField(String bgImage) {
		super();
		
		backgroundImage = bgImage;
        
		drawBackground();
	}
	
	private void drawBackground() {
		BackgroundSize tfBackgroundSize = new BackgroundSize(getPrefWidth(), getPrefHeight(), true, true, true, false);
        BackgroundImage mytfBI= new BackgroundImage(new Image(backgroundImage,getPrefWidth(),getPrefHeight(),false,true),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
                tfBackgroundSize);
        setStyle("-fx-text-inner-color: #c4c4c4; -fx-font-size: 16px; -fx-font-family: '04b03'");
        setAlignment(Pos.CENTER);
        setBackground(new Background(mytfBI));
	}
	
	@Override
	public void setPrefSize(double width,double height) {
		super.setPrefSize(width, height);
		
		// Update UI size
		drawBackground();
	}
}
