package oosd.group10.plutocracywars.view.menu;

import javafx.scene.layout.Pane;
import oosd.group10.plutocracywars.view.PWButton;

public class MenuView extends Pane {
	public PWButton startButton;
	
	public MenuView() {
		super();
		
		// Draw view UI
        drawUI();
        
        // Update UI layout
        updateUI();
	}
	
	private void drawUI() {
		startButton = new PWButton("StartButton.png");
        getChildren().add(startButton);
	}
	
	private void updateUI() {
		startButton.setLayoutX(getPrefWidth()/2-(235/2));
        startButton.setLayoutY(getPrefHeight()-80);
        startButton.setPrefSize(235, 50);
	}
	
	@Override
	public void setPrefSize(double width,double height) {
		super.setPrefSize(width, height);
		
		// Update UI size
		updateUI();
	}
}
