package oosd.group10.plutocracywars.view;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import oosd.group10.plutocracywars.controller.GameSceneController;

public class PWButton extends Button {
	private String bgImage;
	public PWButton(String imageName) {
		super();
		bgImage = imageName;
		
		drawBackground();
	}
	
	private void drawBackground() {
		BackgroundSize backgroundSize = new BackgroundSize(getPrefWidth(), getPrefHeight(), true, true, true, false);
        BackgroundImage myBI= new BackgroundImage(new Image(bgImage,getPrefWidth(),getPrefHeight(),true,true),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
                backgroundSize);
        setBackground(new Background(myBI));
	}
	
	@Override
	public void setPrefSize(double width,double height) {
		super.setPrefSize(width, height);
		
		// Update UI size
		drawBackground();
	}
}
