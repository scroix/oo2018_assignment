package oosd.group10.plutocracywars.view.battle;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import oosd.group10.plutocracywars.model.board.piece.BoardPiece;

public class PieceView extends Rectangle {
    public ImageableBoardPiece imageableBoardPiece;

    private static final int cellPadding = 6;

    public PieceView(BoardPiece boardPiece, int width, int height) {
    	imageableBoardPiece = new ImageableBoardPieceImpl(boardPiece);

        setWidth(width-(cellPadding*2));
        setHeight(height-(cellPadding*2));

        relocate((imageableBoardPiece.getLocation().getX() * width)+cellPadding, (imageableBoardPiece.getLocation().getY() * height)+cellPadding);

        // Make Piece not block clicks and allow CellView to handle
        setMouseTransparent(true);

    	String imageName = imageableBoardPiece.getImageName();
        Image img = new Image(imageName);
        setFill(new ImagePattern(img));
    }
    
    public void DrawShield() {
    	setStroke(Color.SILVER);
    	setStrokeWidth(4);
    }
    
    public void RemoveShield() {
    	setStrokeWidth(0);
    }
}
