package oosd.group10.plutocracywars.view.battle;

public interface Imageable {

	String getImageName();

	String getProfileImageName();

}
