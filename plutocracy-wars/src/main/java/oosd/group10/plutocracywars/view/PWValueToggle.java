package oosd.group10.plutocracywars.view;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

public class PWValueToggle extends Pane {
	// View
	private Label playerUnitCountLabel;
	private Button minusButton;
	private Button plusButton;
	
	public enum DisplayMode {
		DefaultValue,
		SquareValue
	}
	
	// Instance vars
	private int count;
	private int minCount;
	private int maxCount;
	private DisplayMode displayMode;
	
	public PWValueToggle(int currentValue,int min,int max,DisplayMode mode) {
		super();
		
		count = currentValue;
		minCount = min;
		maxCount = max;
		displayMode = mode;
		
		BackgroundSize playerUnitCountLabelBGSize = new BackgroundSize(95, 55, true, true, true, false);
        BackgroundImage playerUnitCountLabelBG= new BackgroundImage(new Image("SizeBackground.png",95,55,true,true),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
                playerUnitCountLabelBGSize);
        playerUnitCountLabel = new Label();
        playerUnitCountLabel.setStyle("-fx-text-inner-color: white; -fx-font-size: 18px; -fx-font-family: '04b03'");
        playerUnitCountLabel.setAlignment(Pos.CENTER);
        playerUnitCountLabel.setTextFill(Color.WHITE);
        playerUnitCountLabel.setPrefSize(95, 55);
        playerUnitCountLabel.setBackground(new Background(playerUnitCountLabelBG));
        
        updateLabel();
        
        BackgroundSize minusBackgroundSize = new BackgroundSize(44, 44, true, true, true, false);
        BackgroundImage minusButtonBI= new BackgroundImage(new Image("MinusButton.png",44,44,true,true),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
                minusBackgroundSize);
        
        minusButton = new Button();
        minusButton.setOnMousePressed(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                if (count > minCount) {
                	count--;
                }
                updateLabel();
            }
        });
        minusButton.setPrefSize(44, 44);
        minusButton.setBackground(new Background(minusButtonBI));
        
        BackgroundSize plusBackgroundSize = new BackgroundSize(44, 44, true, true, true, false);
        BackgroundImage plusButtonBI= new BackgroundImage(new Image("PlusButton.png",44,44,true,true),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
                plusBackgroundSize);
        
        plusButton = new Button();
        plusButton.setOnMousePressed(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                // Handle Plus
            	if (count < maxCount) {
            		count++;
                }
            	updateLabel();
            }
        });
        plusButton.setPrefSize(44, 44);
        plusButton.setBackground(new Background(plusButtonBI));
        
        getChildren().addAll(playerUnitCountLabel,minusButton,plusButton);
        updateUI();
	}
	
	private void updateUI() {
		playerUnitCountLabel.setLayoutX(getPrefWidth()/2-(95/2));
        playerUnitCountLabel.setLayoutY(0);
        minusButton.setLayoutX(getPrefWidth()/2-(95/2)-60);
        minusButton.setLayoutY(getPrefHeight()/2-(44/2));
        plusButton.setLayoutX(getPrefWidth()/2+(95/2)+16);
        plusButton.setLayoutY(getPrefHeight()/2-(44/2));
	}
	
	private void updateLabel() {
		switch (displayMode) {
			case DefaultValue: {
				playerUnitCountLabel.setText(String.format("%s", count));
				break;
			}
			case SquareValue: {
				playerUnitCountLabel.setText(String.format("%sx%s", count,count));
				break;
			}
		}
	}
	
	public int getCurrentValue() {
		return count;
	}
	
	@Override
	public void setPrefSize(double width,double height) {
		super.setPrefSize(width, height);
		
		// Update UI size
		updateUI();
	}
}
