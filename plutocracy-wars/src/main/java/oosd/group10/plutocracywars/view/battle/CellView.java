package oosd.group10.plutocracywars.view.battle;

import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Paint;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import oosd.group10.plutocracywars.model.board.BoardCell;

public class CellView extends Rectangle {

    // Size
    public BoardCell boardCellModel;
    private int cellWidth;
    private int cellHeight;
    private int boardXWidth;
    private int boardYHeight;

    // Piece
    public PieceView currentPlacedPieceView;

    // Enums
    public enum CellViewState {
        None,
        TurnHighlight,
        Selected,
        MovementHighlight,
        TargetHighlight,
        AttackHighlight,
        Ready,
        FriendlyFireHighlight,
        ImmuneHighlight,
        ShieldHighlight
    }
    private CellViewState currentCellState = CellViewState.None;

    public CellView(BoardCell boardCell, int width, int height,int xWidth,int yHeight) {
        boardCellModel = boardCell;
        cellWidth = width;
        cellHeight = height;
        boardXWidth = xWidth;
        boardYHeight = yHeight;
        setWidth(width);
        setHeight(height);

        relocate(boardCell.getX() * width, boardCell.getY() * height);

        setFill(Color.rgb(0, 0, 0, 0.3));

        // Set default state
        SetState(CellViewState.None);
    }

    public void SetState(CellViewState state) {
        currentCellState = state;
        switch (state) {
            case None: {
                setStrokeWidth(3);
                setStrokeType(StrokeType.INSIDE);
                LinearGradient linearGradient = new LinearGradient(-(boardCellModel.getX()*cellWidth), -(boardCellModel.getY()*cellHeight), ((cellWidth*boardXWidth)/2)-boardCellModel.getX()*cellWidth, ((cellHeight*boardYHeight)/2)-boardCellModel.getY()*cellHeight, false, CycleMethod.REFLECT, new Stop(0,new Color(1.0f, 0.64705884f, 0.0f,0.3)),new Stop(1,new Color(0.5019608f, 0.0f, 0.5019608f,0.3)));
                setStroke(linearGradient);
                break;
            }
            case TurnHighlight: {
            		setStrokeWidth(2);
                setStrokeType(StrokeType.INSIDE);
                setStroke(Paint.valueOf("#8ec449"));
            		break;
            }
            case Selected: {
                setStrokeWidth(2);
                setStrokeType(StrokeType.INSIDE);
                setStroke(Paint.valueOf("#eae737"));
                break;
            }
            case MovementHighlight: {
                setStrokeWidth(3);
                setStrokeType(StrokeType.INSIDE);
                setStroke(Paint.valueOf("#8ec449"));
                break;
            }
            case TargetHighlight: {
                setStrokeWidth(3);
                setStrokeType(StrokeType.INSIDE);
                setStroke(Paint.valueOf("#FF7D0A"));
                break;
            }
            case AttackHighlight: {
                setStrokeWidth(3);
                setStrokeType(StrokeType.INSIDE);
                setStroke(Paint.valueOf("#C41F3B"));
                break;
            }
            case Ready: {
                setStrokeWidth(2);
                setStrokeType(StrokeType.INSIDE);
                setStroke(Paint.valueOf("#FFFFFF"));
                break;
            }
            case FriendlyFireHighlight: {
                setStrokeWidth(1);
                setStrokeType(StrokeType.INSIDE);
                setStroke(Paint.valueOf("#FFFFFF"));
                break;
            }
            case ImmuneHighlight: {
                setStrokeWidth(1);
                setStrokeType(StrokeType.INSIDE);
                setStroke(Paint.valueOf("#A330C9"));
                break;
            }
            case ShieldHighlight: {
                setStrokeWidth(3);
                setStrokeType(StrokeType.INSIDE);
                setStroke(Paint.valueOf("#C0C0C0"));
                break;
            }
        }
    }
}
