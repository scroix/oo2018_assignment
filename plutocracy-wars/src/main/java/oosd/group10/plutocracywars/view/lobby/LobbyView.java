package oosd.group10.plutocracywars.view.lobby;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import oosd.group10.plutocracywars.view.PWButton;
import oosd.group10.plutocracywars.view.PWTextField;
import oosd.group10.plutocracywars.view.PWValueToggle;
import oosd.group10.plutocracywars.view.PWValueToggle.DisplayMode;

public class LobbyView extends Pane {
	public Label timeLabel;
	public PWTextField timeLimitTF;
	public PWTextField playerOneNameTF;
	public PWTextField playerTwoNameTF;
	public Label playerUnitsLabel;
	public PWValueToggle playerUnitCountToggle;
	public Label boardSizeLabel;
	public PWValueToggle boardSizeToggle;
	public PWButton startButton;
	
	public LobbyView() {
		super();
		
		// Draw the UI
		drawUI();
	}
	
	private void drawUI() {
		// Time limit UI
		timeLabel = new Label();
        timeLabel.setText("Round time");
        timeLabel.setStyle("-fx-text-inner-color: white; -fx-font-size: 18px; -fx-font-family: '04b03'");
        timeLabel.setAlignment(Pos.CENTER);
        timeLabel.setTextFill(Color.WHITE);
        timeLabel.setPrefSize(120, 40);
        
        timeLimitTF = new PWTextField("SizeBackground.png");
        timeLimitTF.setPrefSize(95, 40);
        timeLimitTF.setText("60");
        
        // Player 1 name UI
        BackgroundSize tfBackgroundSize = new BackgroundSize(237, 40, true, true, true, false);
        BackgroundImage mytfBI= new BackgroundImage(new Image("TextFieldBackground.png",237,40,true,true),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
                tfBackgroundSize);
        playerOneNameTF = new PWTextField("TextFieldBackground.png");
        playerOneNameTF.setPrefSize(237, 40);
        playerOneNameTF.setPromptText("Player 1 name...");
        playerOneNameTF.setStyle("-fx-text-inner-color: #c4c4c4; -fx-font-size: 16px; -fx-font-family: '04b03'");
        playerOneNameTF.setAlignment(Pos.CENTER);
        playerOneNameTF.setBackground(new Background(mytfBI));
        
        // Player 2 name UI
        BackgroundSize tf2BackgroundSize = new BackgroundSize(237, 40, true, true, true, false);
        BackgroundImage mytf2BI= new BackgroundImage(new Image("TextFieldBackground.png",237,40,true,true),
                BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
                tf2BackgroundSize);
        playerTwoNameTF = new PWTextField("TextFieldBackground.png");
        playerTwoNameTF.setPrefSize(237, 40);
        playerTwoNameTF.setPromptText("Player 2 name...");
        playerTwoNameTF.setStyle("-fx-text-inner-color: #c4c4c4; -fx-font-size: 16px; -fx-font-family: '04b03'");
        playerTwoNameTF.setAlignment(Pos.CENTER);
        playerTwoNameTF.setBackground(new Background(mytf2BI));
        
        // Number of units UI
        playerUnitsLabel = new Label();
        playerUnitsLabel.setText("Number of units per player");
        playerUnitsLabel.setStyle("-fx-text-inner-color: white; -fx-font-size: 18px; -fx-font-family: '04b03'");
        playerUnitsLabel.setAlignment(Pos.CENTER);
        playerUnitsLabel.setTextFill(Color.WHITE);
        
        playerUnitCountToggle = new PWValueToggle(3,1,6,DisplayMode.DefaultValue);
        playerUnitCountToggle.setPrefSize(305, 55);
        
        // Board size UI
        boardSizeLabel = new Label();
        boardSizeLabel.setText("Board Size");
        boardSizeLabel.setStyle("-fx-text-inner-color: white; -fx-font-size: 18px; -fx-font-family: '04b03'");
        boardSizeLabel.setAlignment(Pos.CENTER);
        boardSizeLabel.setTextFill(Color.WHITE);
        
        boardSizeToggle = new PWValueToggle(10,6,14,DisplayMode.SquareValue);
        boardSizeToggle.setPrefSize(305, 55);
        
        // Start button
        startButton = new PWButton("StartBattleButton.png");
        startButton.setPrefSize(235, 50);
        
        getChildren().addAll(timeLabel,timeLimitTF,playerOneNameTF,playerTwoNameTF,playerUnitsLabel,playerUnitCountToggle,boardSizeLabel,boardSizeToggle,startButton);
        
        // Update UI layout
        updateUI();
	}
	
	private void updateUI() {
		timeLabel.setLayoutX(getPrefWidth()-120-65);
        timeLabel.setLayoutY((getPrefHeight()-485)+30);
        timeLimitTF.setLayoutX(getPrefWidth()-95-80);
        timeLimitTF.setLayoutY((getPrefHeight()-485)+70);
        playerOneNameTF.setLayoutX(30);
        playerOneNameTF.setLayoutY((getPrefHeight()-485)+30);
        playerTwoNameTF.setLayoutX(30);
        playerTwoNameTF.setLayoutY((getPrefHeight()-485)+80);
        playerUnitsLabel.setLayoutX(0);
        playerUnitsLabel.setLayoutY((getPrefHeight()-485)+160);
        playerUnitsLabel.setPrefSize(getPrefWidth(), 40);
        playerUnitCountToggle.setLayoutX(getPrefWidth()/2-(303/2));
        playerUnitCountToggle.setLayoutY((getPrefHeight()-485)+210);
        boardSizeLabel.setLayoutX(0);
        boardSizeLabel.setLayoutY((getPrefHeight()-485)+280);
        boardSizeLabel.setPrefSize(getPrefWidth(), 40);
        boardSizeToggle.setLayoutX(getPrefWidth()/2-(303/2));
        boardSizeToggle.setLayoutY((getPrefHeight()-485)+330);
        startButton.setLayoutX(getPrefWidth()/2-(235/2));
        startButton.setLayoutY(getPrefHeight()-80);
	}
	
	@Override
	public void setPrefSize(double width,double height) {
		super.setPrefSize(width, height);
		
		// Update UI size
		updateUI();
	}
}
