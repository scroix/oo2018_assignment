package oosd.group10.plutocracywars.view.battle;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import oosd.group10.plutocracywars.model.board.piece.BoardPiece;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnit;
import oosd.group10.plutocracywars.view.PWValueToggle;
import oosd.group10.plutocracywars.view.PWValueToggle.DisplayMode;

public class HUDView extends Pane {
	
	private Text unitText = new Text();
	private Text playerText = new Text();
	private Rectangle unitProfile = new Rectangle();
	
	private int resetCount = 1;
	private List<Node> resetNodes = new ArrayList<Node>();
		
	public HUDView() {
				
		drawBackground();
		createResetNodes();
	
		playerText.setFont(new Font("04b03",16));
		playerText.setFill(Color.LIGHTGRAY);
		playerText.setWrappingWidth(200);
		playerText.setTextAlignment(TextAlignment.CENTER);
		playerText.setLayoutX(160);
		playerText.setLayoutY(65);
		
		unitText.setFont(new Font("04b03",24));
		unitText.setFill(Color.WHITE);
		unitText.setWrappingWidth(200);
		unitText.setTextAlignment(TextAlignment.CENTER);
		unitText.setLayoutX(160);
		unitText.setLayoutY(45);
		
		unitProfile.setWidth(110);
		unitProfile.setHeight(70);
		unitProfile.relocate(390, 15);
		unitProfile.setFill(Color.TRANSPARENT);
		
		getChildren().addAll(playerText, unitText,unitProfile);
		getChildren().addAll(resetNodes);
	}
	
	private void drawBackground() {
		BackgroundSize backgroundSize = new BackgroundSize(getWidth(), getHeight(), true, true, true, false);
        BackgroundImage myBI= new BackgroundImage(new Image("HUD_Background.png",getWidth(),getHeight(),true,true),
                BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.CENTER,
                backgroundSize);
        setBackground(new Background(myBI));
	}
	
	public void updateSelectedUnit(BoardPiece piece) {
		ImageableBoardPiece imageableBoardPiece = new ImageableBoardPieceImpl(piece);
		if(piece instanceof PlayerUnit) {
			PlayerUnit unit = ((PlayerUnit) piece);
			String name = unit.getPlayerUnitType().getLabel();
			String playerName = unit.getOwner().getName();
			String profileImageName = imageableBoardPiece.getImageName();
			
			unitText.setText(name);
			playerText.setText(playerName);
	        Image img = new Image(imageableBoardPiece.getProfileImageName());
	        unitProfile.setFill(new ImagePattern(img));
		}
		else {
			unitText.setText("");
			playerText.setText("");
			unitProfile.setFill(Color.TRANSPARENT);
		}
	}
	
	public void setUnitText(String text) {
		unitText.setText(text);
	}
	
	public void setPlayerText(String text) {
		playerText.setText(text);
	}
	
	public void hideProfileImage() {
		unitProfile.setFill(Color.TRANSPARENT);
	}
	
	public void setResetVisbility(boolean visible) {
		Iterator<Node> resetNodeIterator = resetNodes.iterator();
		while(resetNodeIterator.hasNext()) {
			resetNodeIterator.next().setVisible(visible);
		}
	}
	
	public int getResetCount() {
		return resetCount;
	}
	
	public Node getResetButton() {
		for (Node node : resetNodes) {
			if (node.getId().equals("playerUnitCountLabel")) {
				return node;
			}
		}
		return null;
	}
	
	public void createResetNodes() {
		
		List<Node> nodes = new ArrayList<Node>();

		BackgroundSize resetBGSize = new BackgroundSize(95, 55, true, true, true, false);
		BackgroundImage resetBG = new BackgroundImage(
				new Image("SizeBackground.png", 95, 55, true, true), BackgroundRepeat.NO_REPEAT,
				BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, resetBGSize);
		Label resetCountLabel = new Label();
		resetCountLabel.setText("1");
		
		resetCountLabel.relocate(50, 25);
		resetCountLabel.setStyle("-fx-text-inner-color: white; -fx-font-size: 18px; -fx-font-family: '04b03'");
		resetCountLabel.setAlignment(Pos.CENTER);
		resetCountLabel.setTextFill(Color.WHITE);
		resetCountLabel.setPrefSize(45, 55);
		resetCountLabel.setBackground(new Background(resetBG));
		
		resetCountLabel.setId("playerUnitCountLabel");
		
		resetCountLabel.setOnMouseEntered(new EventHandler<MouseEvent>() {
		    @Override
		    public void handle(MouseEvent event) {
		    	resetCountLabel.setStyle("-fx-font-size: 12px");
		    	resetCountLabel.setText("Reset");
		    } 
		});
		
		resetCountLabel.setOnMouseExited(new EventHandler<MouseEvent>() {
		    @Override
		    public void handle(MouseEvent event) {
		    	resetCountLabel.setStyle("-fx-font-size: 18px");
		    	resetCountLabel.setText(String.valueOf(resetCount));
		    } 
		});
		
		nodes.add(resetCountLabel);

		BackgroundSize minusBackgroundSize = new BackgroundSize(44, 44, true, true, true, false);
		BackgroundImage minusButtonBI = new BackgroundImage(new Image("MinusButton.png", 44, 44, true, true),
				BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, minusBackgroundSize);

		Button minusButton = new Button();
		minusButton.setOnMousePressed(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent me) {
				if (resetCount > 1) {
					resetCount--;
				}
				resetCountLabel.setText(String.format("%s", resetCount));
			}
		});
		minusButton.relocate(15, 38);
		minusButton.setPrefSize(22, 22);
		minusButton.setBackground(new Background(minusButtonBI));
		
		nodes.add(minusButton);

		BackgroundSize plusBackgroundSize = new BackgroundSize(44, 44, true, true, true, false);
		BackgroundImage plusButtonBI = new BackgroundImage(new Image("PlusButton.png", 44, 44, true, true),
				BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, plusBackgroundSize);

		Button plusButton = new Button();
		plusButton.setOnMousePressed(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent me) {
				// Handle Plus
				if (resetCount < 3) {
					resetCount++;
					resetCountLabel.setText(String.format("%s", resetCount));
				}
			}
		});
		plusButton.relocate(108, 38);
		plusButton.setPrefSize(22, 22);
		plusButton.setBackground(new Background(plusButtonBI));
		
		nodes.add(plusButton);
		
		Label resetLabel = new Label();
		resetLabel.setText("Reset turns");
		resetLabel.setStyle("-fx-text-inner-color: white; -fx-font-size: 16px; -fx-font-family: '04b03'");
		resetLabel.setAlignment(Pos.CENTER);
		resetLabel.setTextFill(Color.WHITE);
		resetLabel.setPrefSize(120, 40);
		resetLabel.relocate(10, 0);
		
		nodes.add(resetLabel);

		resetNodes = nodes;
	}
}
