package oosd.group10.plutocracywars.view.battle;

import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import oosd.group10.plutocracywars.model.board.Board;
import oosd.group10.plutocracywars.model.board.piece.BoardPiece;

public class BoardView extends Pane{

    // Size
    private int boardXWidth;
    private int boardYHeight;
    private int boardWidth;
    private int boardHeight;

    // Board
    private Board boardModel;
    public CellView[][] boardCells;
    private Group tileGroup = new Group();
    private Group pieceGroup = new Group();

    public BoardView(Board board,int width,int height,int XWidth, int YWidth) {
        boardModel = board;
        boardCells = new CellView[XWidth][YWidth];
        boardXWidth = XWidth;
        boardYHeight = YWidth;
        boardWidth = width;
        boardHeight = height;
        tileGroup.resize(width/XWidth,height/YWidth);
        tileGroup.setLayoutX(0);
        tileGroup.setLayoutY(0);
        pieceGroup.resize(width/XWidth,height/YWidth);
        pieceGroup.setLayoutX(0);
        pieceGroup.setLayoutY(0);
        getChildren().addAll(tileGroup,pieceGroup);

        for (int y = 0; y < YWidth; y++) {
            for (int x = 0; x < XWidth; x++) {
                CellView cell = new CellView(boardModel.getBoardCellByXAndYCoordinates(x,y),width/XWidth,height/YWidth,XWidth,YWidth);
                boardCells[x][y] = cell;

                tileGroup.getChildren().add(cell);
            }
        }
    }

    public void drawPiece(BoardPiece pieceModel) {
        PieceView piece = new PieceView(pieceModel,boardWidth/boardXWidth,boardHeight/boardYHeight);
        piece.setId(pieceModel.toString());
        
        boardCells[pieceModel.getLocation().getX()][pieceModel.getLocation().getY()].currentPlacedPieceView = piece;
        pieceGroup.getChildren().add(piece);
    }
    
    public void clearPieces() {
    	pieceGroup.getChildren().clear();
    }
    
    public void removePiece(BoardPiece pieceModel) {
		PieceView piece = boardCells[pieceModel.getLocation().getX()][pieceModel.getLocation().getY()].currentPlacedPieceView;
		pieceGroup.getChildren().remove(piece);
    }
    
    public void DrawShield(BoardPiece pieceModel) {
    	
    	String pieceId = pieceModel.toString();
    	
    	for (Node node : pieceGroup.getChildren()) {
    		if (node.getId().equals(pieceId)) {
    			((PieceView) node).DrawShield();
    			break;
    		}
    	}
    }
    
    public void RemoveShield(BoardPiece pieceModel) {
    	String pieceId = pieceModel.toString();
    	
    	for (Node node : pieceGroup.getChildren()) {
    		if (node.getId().equals(pieceId)) {
    			((PieceView) node).RemoveShield();
    			break;
    		}
    	}
    }
}
