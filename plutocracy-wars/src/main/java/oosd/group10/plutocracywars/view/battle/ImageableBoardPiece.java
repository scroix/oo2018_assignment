package oosd.group10.plutocracywars.view.battle;

import oosd.group10.plutocracywars.model.board.piece.BoardPiece;

/**
 * A decorator to add view related image functionality to a board piece.
 * 
 * This provides further decoupling between the model and view since it removes the requirement for image names to be stored within the model.
 * Different decorators can also provide different image references, enabling customisation of the UI.
 */
public interface ImageableBoardPiece extends BoardPiece, Imageable {

}
