package oosd.group10.plutocracywars.model.game;

import java.util.List;
import java.util.Set;

import com.google.java.contract.Requires;

import oosd.group10.plutocracywars.model.board.Board;
import oosd.group10.plutocracywars.model.board.piece.Obstacle;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnit;
import oosd.group10.plutocracywars.model.board.piece.unit.UnitActionObserver;

/**
 * Represents the state of the game including board and players. Also handles
 * behaviour of player turns.
 */
public interface Game extends TurnObservable, UnitActionObserver {

	Board getBoard();

	void setBoard(Board board);
	
	/**
	 * @return Number of units per player
	 */
	int getNumberOfUnitsPerPlayer();

	/**
	 * @return A list of players with ordering based on when they were added to the
	 *         game.
	 */
	List<Player> getPlayers();

	/**
	 * Add a new player to the existing players.
	 * 
	 * @param player
	 */
	void addPlayer(Player player);

	/**
	 * @return The player whose turn it is.
	 */
	Player getCurrentPlayer();

	/**
	 * @return The set of all units owned by all players.
	 */
	Set<PlayerUnit> getAllPlayerUnits();

	int numberOfResettableTurns();

	@Requires("player != null")
	boolean canResetTurn(Player player);

	@Requires("canResetTurn(player) && numberOfTurns <= numberOfResettableTurns()")
	void resetTurn(Player player, int numberOfTurns);

	/**
	 * @return Whether a victory condition has been met and the game is complete.
	 */
	boolean isFinished();

	/**
	 * @return The winner of the game
	 */
	@Requires("isFinished()")
	Player getWinner();

	/**
	 * @return The turn limit in seconds.
	 */
	int getTurnTimeInSeconds();

	/**
	 * Set the turn limit. Setting to 0 disables the limit. Can be set after the game is initialised.
	 * 
	 * @param turnTimeInSeconds
	 */
	@Requires("turnTimeInSeconds >= 0")
	void setTurnTimeInSeconds(int turnTimeInSeconds);
	
	void addObstacle(Obstacle obstacle);
}
