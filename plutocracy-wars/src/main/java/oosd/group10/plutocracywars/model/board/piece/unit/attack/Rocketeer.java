package oosd.group10.plutocracywars.model.board.piece.unit.attack;

import static oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitType.ROCKETEER;

import oosd.group10.plutocracywars.model.board.BoardCell;
import oosd.group10.plutocracywars.model.board.piece.TechnologyType;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitType;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitVisitor;
import oosd.group10.plutocracywars.model.board.piece.unit.pattern.CellPattern;
import oosd.group10.plutocracywars.model.board.piece.unit.pattern.SparseSquarePattern;

public class Rocketeer extends AbstractAttackingUnit {

	private static final long serialVersionUID = 3204596331811595853L;
	private static final CellPattern ATTACK_PATTERN = new SparseSquarePattern();
	private static final CellPattern MOVEMENT_PATTERN = new SparseSquarePattern();
	private static final PlayerUnitType PLAYER_UNIT_TYPE = ROCKETEER;

	public Rocketeer(BoardCell location, TechnologyType technologyType) {
		super(location, technologyType);
	}

	@Override
	public CellPattern getAttackPattern() {
		return ATTACK_PATTERN;
	}

	@Override
	public CellPattern getMovementPattern() {
		return MOVEMENT_PATTERN;
	}

	@Override
	public PlayerUnitType getPlayerUnitType() {
		return PLAYER_UNIT_TYPE;
	}
	
	@Override
	public void accept(PlayerUnitVisitor playerUnitVisitor) {
		playerUnitVisitor.visit(this);
	}

}
