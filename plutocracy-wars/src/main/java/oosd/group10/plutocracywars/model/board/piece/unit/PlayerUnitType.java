package oosd.group10.plutocracywars.model.board.piece.unit;

public enum PlayerUnitType {
	//@formatter:off
	RIFLEMAN("Rifleman"),
	BESERKER("Beserker"),
	ROCKETEER("Rocketeer"),
	CHARGER("Charger"),
	JUGGERNAUT("Juggernaut"),
	SPECOPS("SpecOps");
	//@formatter:on

	private final String label;

	PlayerUnitType(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	@Override
	public String toString() {
		return label;
	}

}
