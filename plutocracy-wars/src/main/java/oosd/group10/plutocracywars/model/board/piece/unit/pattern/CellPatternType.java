package oosd.group10.plutocracywars.model.board.piece.unit.pattern;

public enum CellPatternType {
	PLUS,
	LONG_PLUS,
	BLOCKABLE_LONG_PLUS,
	SQUARE,
	SPARSE_SQUARE;
}
