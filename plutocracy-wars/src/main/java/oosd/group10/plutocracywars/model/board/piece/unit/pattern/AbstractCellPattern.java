package oosd.group10.plutocracywars.model.board.piece.unit.pattern;

import java.io.Serializable;

import oosd.group10.plutocracywars.model.board.BoardCell;

public abstract class AbstractCellPattern implements CellPattern, Serializable {

	private static final long serialVersionUID = 2747264888044044108L;

	public AbstractCellPattern() {
	}

	protected static boolean offsetResultsInCurrentLocation(int xOffset, int yOffset) {
		return xOffset == 0 && yOffset == 0;
	}

	protected static boolean offsetResultsInInvalidLocation(BoardCell currentLocation, int xOffset, int yOffset) {
		int currentX = currentLocation.getX();
		int currentY = currentLocation.getY();
		int maxX = currentLocation.getMaxXCoordinate();
		int maxY = currentLocation.getMaxYCoordinate();

		boolean coordinatesAreBelowMinimum = currentX + xOffset < 0 || currentY + yOffset < 0;
		boolean coordinatesAreAboveMaximum = currentX + xOffset > maxX || currentY + yOffset > maxY;

		return coordinatesAreBelowMinimum || coordinatesAreAboveMaximum;
	}

}
