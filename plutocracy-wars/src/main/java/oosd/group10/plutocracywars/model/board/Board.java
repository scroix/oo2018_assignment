package oosd.group10.plutocracywars.model.board;

import java.io.Serializable;
import java.util.List;

import com.google.java.contract.Invariant;
import com.google.java.contract.Requires;

@Invariant({"getColumns() >= 6", "getRows() >= 6"})
public interface Board extends Serializable {
	int getColumns();

	int getRows();

	/**
	 * @param x
	 * @param y
	 * @return Whether the co-ordinate is valid i.e. both x and y are within the bounds of the board.
	 */
	boolean areXAndYCoordinatesValid(int x, int y);

	/**
	 * @param x
	 * @return Whether the co-ordinate is valid i.e. x is within the bounds of the board.
	 */
	boolean isXCoordinateValid(int x);

	/**
	 * @param y
	 * @return Whether the co-ordinate is valid i.e. x is within the bounds of the board.
	 */
	boolean isYCoordinateValid(int y);

	@Requires("areXAndYCoordinatesValid(x, y)")
	BoardCell getBoardCellByXAndYCoordinates(int x, int y);
	
	/**
	 * @return A list of all board cells in sequential order.
	 */
	List<BoardCell> getBoardCells();
}
