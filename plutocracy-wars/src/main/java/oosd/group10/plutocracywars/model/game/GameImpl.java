package oosd.group10.plutocracywars.model.game;

import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import oosd.group10.plutocracywars.model.board.Board;
import oosd.group10.plutocracywars.model.board.piece.Obstacle;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnit;

public class GameImpl implements Game, Serializable {

	private static final long serialVersionUID = -6440643749904621172L;

	private static final Logger LOG = LogManager.getLogger(GameImpl.class);

	private static final int DEFAULT_TURN_TIME_IN_SECONDS = 60;
	private static final int MAX_RESETABBLE_TURNS = 3;

	private Set<Player> playersWhoHaveReset = new HashSet<>();
	private GameState gameState = new GameState();
	private Deque<GameState> gameStates = new ArrayDeque<>();

	private transient TurnScheduler turnScheduler;
	private int turnTimeInSeconds = DEFAULT_TURN_TIME_IN_SECONDS;

	private int numberOfUnitsPerPlayer;

	private transient List<TurnObserver> turnObservers;

	public GameImpl() {
		Player playerOne = new HumanPlayer("Player one", this);
		Player playerTwo = new HumanPlayer("Player two", this);
		numberOfUnitsPerPlayer = 6;
		addPlayer(playerOne);
		addPlayer(playerTwo);
		gameState.setCurrentPlayer(playerOne);

		resetTurnScheduler();
	}

	public GameImpl(String playerOneName, String playerTwoName, int numberOfUnits, int turnTime) {
		playerTwoName = playerOneName.equals(playerTwoName) ? (playerTwoName + "'s Evil Twin") : playerTwoName;
		Player playerOne = new HumanPlayer(playerOneName, this);
		Player playerTwo = new HumanPlayer(playerTwoName, this);
		numberOfUnitsPerPlayer = numberOfUnits;
		turnTimeInSeconds = turnTime;
		addPlayer(playerOne);
		addPlayer(playerTwo);
		gameState.setCurrentPlayer(playerOne);

		resetTurnScheduler();
	}

	public GameImpl(int turnTimeInSeconds) {
		this.turnTimeInSeconds = turnTimeInSeconds;
		resetTurnScheduler();
	}

	public int getNumberOfUnitsPerPlayer() {
		return numberOfUnitsPerPlayer;
	}

	public Board getBoard() {
		return gameState.getBoard();
	}

	public void setBoard(Board board) {
		gameState.setBoard(board);
	}

	public List<Player> getPlayers() {
		return Collections.unmodifiableList(gameState.getPlayers());
	}

	public void addPlayer(Player player) {
		gameState.getPlayers().add(player);
	}

	@Override
	public void unitActionOccurred(PlayerUnit playerUnit) {
		if (!playerUnit.getOwner().equals(getCurrentPlayer())) {
			throw new TurnViolationException(String.format("%s took an action when it was %s's turn.",
					playerUnit.getOwner().getName(), getCurrentPlayer().getName()));
		}
		nextTurn();
	}

	@Override
	public Player getCurrentPlayer() {
		return gameState.getCurrentPlayer();
	}

	@Override
	public Set<PlayerUnit> getAllPlayerUnits() {
		Set<PlayerUnit> allUnits = new HashSet<PlayerUnit>();
		for (Player player : getPlayers()) {
			allUnits.addAll(player.getUnits());
		}
		return allUnits;
	}

	@Override
	public void attachTurnObserver(TurnObserver turnObserver) {
		if (turnObservers == null) {
			turnObservers = new ArrayList<TurnObserver>();
		}
		LOG.debug("Attaching turn observer: {}", turnObserver);
		turnObservers.add(turnObserver);
	}

	@Override
	public void detachTurnObserver(TurnObserver turnObserver) {
		if (turnObservers == null) {
			return;
		}
		LOG.debug("Detaching turn observer: {}", turnObserver);
		turnObservers.remove(turnObserver);
	}

	public void notifyNextTurnOccurred() {
		if (turnObservers != null) {
			for (TurnObserver turnObserver : turnObservers) {
				LOG.debug("Notifying turn observer: {}", turnObserver);
				turnObserver.nextTurnOccurred();
			}
		}
	}

	// Function discovered courtesy of WolframAlpha
	// http://www.wolframalpha.com/input/?i=0,0,1,1,2,2,3,3
	@Override
	public int numberOfResettableTurns() {
		int trueNumberOfResettableTurns = (int) (1 / 4f
				* (2 * gameStates.size() + Math.pow(-1, gameStates.size() + 1) - 3));

		return (trueNumberOfResettableTurns > MAX_RESETABBLE_TURNS) ? MAX_RESETABBLE_TURNS
				: trueNumberOfResettableTurns;
	}

	@Override
	public boolean canResetTurn(Player player) {
		return numberOfResettableTurns() > 0 && !playersWhoHaveReset.contains(player);
	}

	@Override
	public void resetTurn(Player player, int numberOfTurns) {
		playersWhoHaveReset.add(player);

		gameState.flush(this);

		for (int i = 0; i < numberOfTurns * 2; i++) {
			gameStates.pop();
		}

		gameState = gameStates.peek();

		gameState.reattach(this);
	}

	@Override
	public boolean isFinished() {
		for (Player player : gameState.getPlayers()) {
			if (player.getUnits().isEmpty()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Player getWinner() {
		return getCurrentPlayer();
	}

	@Override
	public int getTurnTimeInSeconds() {
		return turnTimeInSeconds;
	}

	@Override
	public void setTurnTimeInSeconds(int turnTimeInSeconds) {
		this.turnTimeInSeconds = turnTimeInSeconds;
	}

	@Override
	public void addObstacle(Obstacle obstacle) {
		if (obstacle instanceof TurnObserver) {
			this.attachTurnObserver((TurnObserver) obstacle);
		}
		gameState.getObstacles().add(obstacle);
	}

	protected GameState getGameState() {
		return gameState;
	}

	protected void setGameState(GameState gameState) {
		this.gameState = gameState;
		gameStates.push(gameState);
	}

	protected synchronized void nextTurn() {
		resetTurnScheduler();

		if (isFinished() && turnScheduler != null) {
			turnScheduler.cancelScheduledNextTurn();
			return;
		}

		switchPlayers();

		if (isAllTurnsExhausted()) {
			nextRound();
		}

		saveGameState();

		notifyNextTurnOccurred();
	}

	private void nextRound() {
		for (Player player : getPlayers()) {
			for (PlayerUnit playerUnit : player.getUnits()) {
				playerUnit.resetForNextRound();
			}
		}
	}

	private boolean currentPlayerIsLastPlayer() {
		return getPlayers().indexOf(getCurrentPlayer()) == getPlayers().size() - 1;
	}

	private int nextPlayerIndex() {
		return getPlayers().indexOf(getCurrentPlayer()) + 1;
	}

	private boolean isAllTurnsExhausted() {
		for (Player player : getPlayers()) {
			if (isPlayerAbleToTakeTurn(player)) {
				return false;
			}
		}

		return true;
	}

	private boolean isPlayerAbleToTakeTurn(Player player) {
		return !player.getUnitsAbleToMove().isEmpty();
	}

	private void resetTurnScheduler() {
		if (turnScheduler != null) {
			turnScheduler.cancelScheduledNextTurn();
		}

		if (turnTimeInSeconds > 0) {
			turnScheduler = new TurnScheduler(() -> nextTurn(), turnTimeInSeconds);
		} else {
			turnScheduler = null;
		}
	}

	private void switchPlayers() {
		if (currentPlayerIsLastPlayer()) {
			gameState.setCurrentPlayer(getPlayers().get(0));
		} else {
			gameState.setCurrentPlayer(getPlayers().get(nextPlayerIndex()));
		}
	}

	private void saveGameState() {
		gameStates.push(gameState.deepClone());
	}

}
