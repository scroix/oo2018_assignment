package oosd.group10.plutocracywars.model.board.piece.unit.attack;

import java.util.Set;

import com.google.java.contract.Requires;

import oosd.group10.plutocracywars.model.board.BoardCell;

public interface Attacker {

	@Requires("canAttack(boardCellToAttack)")
	void attack(BoardCell boardCellToAttack);

	/**
	 * @param boardCell
	 * @return True if the attacker object can reach the board cell, has not yet
	 *         attacked, and the board cell contains a board piece of an
	 *         attackable technology type. Otherwise false.
	 */
	boolean canAttack(BoardCell boardCell);

	/**
	 * @return The set of all board cells than can be reached by the attacker
	 *         object and contain board pieces of an attackable technology type.
	 *         If the attacker has already attacked this round, the set will be
	 *         empty.
	 */
	Set<BoardCell> getBoardCellsCanAttack();

	/**
	 * @return True if the attacker has attacked this round. Otherwise false.
	 */
	boolean hasAttacked();

	void resetForNextRound();
	
	void setShieldStatus(boolean status, boolean quietly);
	
	boolean getShieldStatus();

}
