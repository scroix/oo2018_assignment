package oosd.group10.plutocracywars.model.game;

public interface TurnObserver {

	void nextTurnOccurred();

}	

