package oosd.group10.plutocracywars.model.board.piece.unit.attack;

import static oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitType.RIFLEMAN;

import oosd.group10.plutocracywars.model.board.BoardCell;
import oosd.group10.plutocracywars.model.board.piece.TechnologyType;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitType;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitVisitor;
import oosd.group10.plutocracywars.model.board.piece.unit.pattern.CellPattern;
import oosd.group10.plutocracywars.model.board.piece.unit.pattern.LongPlusPattern;
import oosd.group10.plutocracywars.model.board.piece.unit.pattern.SquarePattern;

public class Rifleman extends AbstractAttackingUnit {

	private static final long serialVersionUID = -5928270662848385697L;
	private static final CellPattern MOVEMENT_PATTERN = new SquarePattern();
	private static final CellPattern ATTACK_PATTERN = new LongPlusPattern();
	private static final PlayerUnitType PLAYER_UNIT_TYPE = RIFLEMAN;

	public Rifleman(BoardCell location, TechnologyType technologyType) {
		super(location, technologyType);
	}

	@Override
	public CellPattern getAttackPattern() {
		return ATTACK_PATTERN;
	}

	@Override
	public CellPattern getMovementPattern() {
		return MOVEMENT_PATTERN;
	}

	@Override
	public PlayerUnitType getPlayerUnitType() {
		return PLAYER_UNIT_TYPE;
	}

	@Override
	public void accept(PlayerUnitVisitor playerUnitVisitor) {
		playerUnitVisitor.visit(this);
	}

}
