package oosd.group10.plutocracywars.model.board.piece.unit.pattern;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import oosd.group10.plutocracywars.model.board.BoardCell;

public class BlockableLongPlusPattern extends LongPlusPattern {

	private static final long serialVersionUID = 7194991240547062405L;

	@Override
	public Set<BoardCell> getCellsInPattern(BoardCell currentLocation) {
		Set<BoardCell> cellsInPattern = super.getCellsInPattern(currentLocation);
		return getPrunedPattern(currentLocation, cellsInPattern);
	}

	private Set<BoardCell> getPrunedPattern(BoardCell currentLocation, Set<BoardCell> cellsInPattern) {
		if (cellsInPattern.isEmpty()) {
			return cellsInPattern;
		}
		int minX = 0;
		int minY = 0;
		int maxX = currentLocation.getMaxXCoordinate();
		int maxY = currentLocation.getMaxYCoordinate();

		for (BoardCell boardCell : cellsInPattern) {
			if (isBoardPieceBlockingCell(boardCell)) {
				if (isOnHorizontalPath(currentLocation, boardCell)) {
					if (resultsInNewMinimumX(currentLocation, boardCell, minX)) {
						minX = boardCell.getX() + 1;
					}
					if (resultsInNewMaximumX(currentLocation, boardCell, maxX)) {
						maxX = boardCell.getX() - 1;
					}
				}
				if (isOnVerticalPath(currentLocation, boardCell)) {
					if (resultsInNewMinimumY(currentLocation, boardCell, minY)) {
						minY = boardCell.getY() + 1;
					}
					if (resultsInNewMaximumY(currentLocation, boardCell, maxY)) {
						maxY = boardCell.getY() - 1;
					}
				}
			}
		}

		return getPrunedPatternBasedOnMinXYAndMaxXY(currentLocation, cellsInPattern, minX, minY, maxX, maxY);
	}

	private boolean isBoardPieceBlockingCell(BoardCell boardCell) {
		return boardCell.getOccupant() != null;
	}

	private boolean isOnHorizontalPath(BoardCell currentLocation, BoardCell boardCell) {
		return currentLocation.getY() == boardCell.getY();
	}

	private boolean isOnVerticalPath(BoardCell currentLocation, BoardCell boardCell) {
		return currentLocation.getX() == boardCell.getX();
	}

	private boolean resultsInNewMinimumX(BoardCell currentLocation, BoardCell boardCell, int currentMinX) {
		return boardCell.getX() < currentLocation.getX() && boardCell.getX() >= currentMinX;
	}

	private boolean resultsInNewMinimumY(BoardCell currentLocation, BoardCell boardCell, int currentMinY) {
		return boardCell.getY() < currentLocation.getY() && boardCell.getY() >= currentMinY;
	}

	private boolean resultsInNewMaximumX(BoardCell currentLocation, BoardCell boardCell, int currentMaxX) {
		return boardCell.getX() > currentLocation.getX() && boardCell.getX() <= currentMaxX;
	}

	private boolean resultsInNewMaximumY(BoardCell currentLocation, BoardCell boardCell, int currentMaxY) {
		return boardCell.getY() > currentLocation.getY() && boardCell.getY() <= currentMaxY;
	}

	private Set<BoardCell> getPrunedPatternBasedOnMinXYAndMaxXY(BoardCell currentLocation, Set<BoardCell> cellsToPrune, int minX, int minY, int maxX,
			int maxY) {
		Set<BoardCell> pruned = cellsToPrune.stream().filter(boardCell -> {
			if (isOnHorizontalPath(currentLocation, boardCell)) {
				return boardCell.getX() >= minX && boardCell.getX() <= maxX;
			} else if (isOnVerticalPath(currentLocation, boardCell)) {
				return boardCell.getY() >= minY && boardCell.getY() <= maxY;
			} else {
				throw new IllegalStateException("Path must be either horizontal or vertical.");
			}
		}).collect(Collectors.toCollection(HashSet::new));

		return pruned;
	}
}
