package oosd.group10.plutocracywars.model.game;

import static java.lang.String.format;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import oosd.group10.plutocracywars.model.board.piece.unit.Moveable;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnit;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitCollectionVisitor;
import oosd.group10.plutocracywars.model.board.piece.unit.attack.Attacker;

public class HumanPlayer implements Player {

	private static final long serialVersionUID = -1340415690639764409L;

	private static int nextId = 1;

	private transient Game game;
	private String name;
	private Set<PlayerUnit> units = new HashSet<PlayerUnit>();;
	private int id;

	public HumanPlayer(String name, Game game) {
		id = nextId++;
		this.name = name;
		this.game = game;
	}

	public Set<PlayerUnit> getUnits() {
		return Collections.unmodifiableSet(units);
	}

	public void addUnit(PlayerUnit unit) {
		unit.setOwner(this);
		unit.attachUnitActionObserver(game);
		units.add(unit);
	}

	public void removeUnit(PlayerUnit unit) {
		throwExceptionIfUnitNotOwnedByPlayer(unit);
		unit.setOwner(null);
		unit.detachUnitActionObserver(game);
		units.remove(unit);
	}

	public void setUnits(Set<PlayerUnit> units) {
		this.units = units;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void reattach(Game game) {
		this.game = game;
		for (PlayerUnit unit : units) {
			unit.attachUnitActionObserver(game);
		}
	}

	@Override
	public Set<Attacker> getUnitsAbleToAttack() {
		PlayerUnitCollectionVisitor visitor = new PlayerUnitCollectionVisitor();
		for (PlayerUnit unit : units) {
			unit.accept(visitor);
		}
		return visitor.getAttackingUnits().stream().filter(attacker -> !attacker.hasAttacked())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<Moveable> getUnitsAbleToMove() {
		return units.stream().filter(unit -> !unit.hasMoved()).collect(Collectors.toSet());
	}

	private void throwExceptionIfUnitNotOwnedByPlayer(PlayerUnit unit) {
		if (!units.contains(unit)) {
			throw new IllegalArgumentException(
					format("Unit is not owned by this player: player=%s; unit=%s;", this.name, unit));
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HumanPlayer other = (HumanPlayer) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
