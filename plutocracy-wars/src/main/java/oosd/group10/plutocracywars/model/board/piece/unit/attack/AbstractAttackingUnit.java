package oosd.group10.plutocracywars.model.board.piece.unit.attack;

import static java.lang.String.format;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import oosd.group10.plutocracywars.model.board.BoardCell;
import oosd.group10.plutocracywars.model.board.piece.BoardPiece;
import oosd.group10.plutocracywars.model.board.piece.TechnologyType;
import oosd.group10.plutocracywars.model.board.piece.unit.AbstractPlayerUnit;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitCollectionVisitor;
import oosd.group10.plutocracywars.model.board.piece.unit.pattern.CellPattern;

public abstract class AbstractAttackingUnit extends AbstractPlayerUnit implements Attacker {

	private static final long serialVersionUID = -6621119953790898159L;

	private static final Logger LOG = LogManager.getLogger(AbstractAttackingUnit.class);

	private boolean hasAttacked;
	
	private boolean hasShield = false;

	protected AbstractAttackingUnit(BoardCell location, TechnologyType technologyType) {
		super(location, technologyType);
	}

	@Override
	public void attack(BoardCell boardCellToAttack) {
		 if (canAttack(boardCellToAttack)) {

			LOG.debug("{} attacked {}", this, boardCellToAttack.getOccupant());
	
			boardCellToAttack.getOccupant().kill();
		 }
		setHasAttacked(true);

		notifyAttackOccurred();
	}

	@Override
	public boolean canAttack(BoardCell boardCell) {
		return !hasAttacked() && getBoardCellsCanAttack().contains(boardCell) && hasKillablePiece(boardCell);
	}

	@Override
	public Set<BoardCell> getBoardCellsCanAttack() {
		Set<BoardCell> boardCellsCanAttack = new HashSet<BoardCell>();
		if (hasAttacked) {
			return boardCellsCanAttack;
		}

		for (BoardCell cellInRangeOfAttack : getAttackPattern().getCellsInPattern(getLocation())) {
			LOG.debug("cellInRangeOfAttack {}", cellInRangeOfAttack);
			if (getKillablePiece(cellInRangeOfAttack) != null) {
				LOG.debug("cell {} has killable pieces", cellInRangeOfAttack);
				boardCellsCanAttack.add(cellInRangeOfAttack);
			}
		}

		return boardCellsCanAttack;
	}

	@Override
	public void resetForNextRound() {
		super.resetForNextRound();
		setHasAttacked(false);
	}

	@Override
	public boolean hasAttacked() {
		return hasAttacked;
	}

	public void setHasAttacked(boolean hasAttacked) {
		this.hasAttacked = hasAttacked;
	}

	public abstract CellPattern getAttackPattern();

	public void accept(PlayerUnitCollectionVisitor visitor) {
		visitor.visit(this);
	}

	protected void throwExceptionIfNotAttackable(BoardCell boardCellToAttack) {
		if (!canAttack(boardCellToAttack)) {
			throw new IllegalArgumentException(
					format("Cannot attack board cell: boardCellToAttack=%s; currentLocation=%s; hasAttacked=%s;",
							boardCellToAttack, getLocation(), hasAttacked));
		}
	}

	public void notifyAttackOccurred() {
		notifyUnitActionOccurred();
	}
	
	@Override
	protected void notifyMovedOccurred() {
		
	}

	private BoardPiece getKillablePiece(BoardCell boardCell) {
		if (hasKillablePiece(boardCell)) {
			return boardCell.getOccupant();
		}

		return null;
	}

	private boolean hasKillablePiece(BoardCell boardCell) {
		BoardPiece occupant = boardCell.getOccupant();
		return occupant != null && occupant.isDefeatedBy(getTechnologyType());
	}
	
	@Override
	public void setShieldStatus(boolean status, boolean quietly) {
		this.hasShield = status;
		if (!quietly) {
			setHasAttacked(true);
			notifyAttackOccurred();
		}
	}

	@Override
	public boolean getShieldStatus() {
		return hasShield;
	}

}
