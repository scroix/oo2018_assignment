package oosd.group10.plutocracywars.model.board.piece.unit;

import oosd.group10.plutocracywars.model.board.BoardCell;
import oosd.group10.plutocracywars.model.board.piece.TechnologyType;

/* An abstract factory pattern was chosen to create the various player unit types. The provided the following benefits:
 * - Removed the need for the client to know about implementation of unit types i.e. client doesn't know or care about Beserker, only PlayerUnit.
 * - Allowed us to make changes to the way player units are constructed as long as we did not tighten the contract any further.
 *   For example, rather than having separate classes for each unit type we could re-implement them as a generic class that takes a movement pattern and attack pattern.
 *   This only reason this wasn't done was because it was felt separate classes were more expressible and readily extensible. 
 */
public interface PlayerUnitFactory {
	PlayerUnit createPlayerUnit(BoardCell location, PlayerUnitType playerUnitType, TechnologyType technologyType);
}
