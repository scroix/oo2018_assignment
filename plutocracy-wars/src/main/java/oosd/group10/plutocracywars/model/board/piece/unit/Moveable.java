package oosd.group10.plutocracywars.model.board.piece.unit;

import java.util.Set;

import com.google.java.contract.Requires;

import oosd.group10.plutocracywars.model.board.BoardCell;

public interface Moveable {

	@Requires("canMoveTo(newLocation)")
	void moveTo(BoardCell newLocation);

	/**
	 * @param boardCell
	 * @return True if the moveable object can reach the board cell and has not
	 *         yet moved. Otherwise false.
	 */
	boolean canMoveTo(BoardCell boardCell);

	/**
	 * @return The set of all board cells than can be reached by the moveable
	 *         object. If the moveable object has already moved this round, the
	 *         set will be empty.
	 */
	Set<BoardCell> getBoardCellsCanMoveTo();

	/**
	 * @return True if the moveable has been moved this round. Otherwise false.
	 */
	boolean hasMoved();

	void resetForNextRound();

}