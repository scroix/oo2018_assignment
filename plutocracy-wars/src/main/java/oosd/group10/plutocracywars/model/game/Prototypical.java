package oosd.group10.plutocracywars.model.game;

public interface Prototypical<T> {
	T deepClone();
}
