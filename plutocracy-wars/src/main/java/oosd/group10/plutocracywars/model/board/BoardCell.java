package oosd.group10.plutocracywars.model.board;

import java.io.Serializable;

import com.google.java.contract.Requires;

import oosd.group10.plutocracywars.model.board.piece.BoardPiece;

public interface BoardCell extends Serializable {

	int getX();

	int getY();

	/**
	 * @return True if the cell contains a board piece. Otherwise false.
	 */
	boolean isOccupied();

	@Requires("!isOccupied()")
	void receive(BoardPiece boardPiece);

	void remove(BoardPiece boardPiece);

	/**
	 * @param boardPiece
	 * @return True if the cell contains the provided board piece. Otherwise false.
	 */
	boolean contains(BoardPiece boardPiece);

	/**
	 * @return The current board piece occupying the cell, or null if unoccupied.
	 */
	BoardPiece getOccupant();

	int getMaxXCoordinate();

	int getMaxYCoordinate();

	/**
	 * @param xOffset
	 * @param yOffset
	 * @return The board cell at the co-ordinates of this board cell, plus or minus the offset. The co-ordinates must be valid after applying any offset.
	 */
	BoardCell getNeighbourByXAndYOffset(int xOffset, int yOffset);
	
	boolean areXAndYOffsetsValid(int xOffset, int yOffset);

	Board getBoard();

}
