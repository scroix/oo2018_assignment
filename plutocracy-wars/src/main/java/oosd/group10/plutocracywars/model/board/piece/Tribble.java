package oosd.group10.plutocracywars.model.board.piece;

import static org.apache.commons.lang3.RandomUtils.nextBoolean;
import static org.apache.commons.lang3.RandomUtils.nextInt;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import oosd.group10.plutocracywars.model.board.BoardCell;
import oosd.group10.plutocracywars.model.game.TurnObserver;

public class Tribble extends Obstacle implements TurnObserver {

	private static final long serialVersionUID = 3504367512837310588L;
	private static final long PERIMETER_SIZE = 9;
	private static final Logger LOG = LogManager.getLogger(Tribble.class);

	private static int numberOfTribbles = 0;

	private Tribble parent;
	private Tribble child;
	private int tribbleId;

	public Tribble(BoardCell location) {
		super(location, null);
		tribbleId = ++numberOfTribbles;
	}

	private Tribble(BoardCell location, Tribble parent) {
		this(location);
		this.parent = parent;
	}

	@Override
	public boolean isDefeatedBy(TechnologyType technologyType) {
		return true;
	}

	@Override
	public void nextTurnOccurred() {
		if (getLocation() != null) {
			if (child == null) {
				spawnChild();
			} else {
				LOG.debug("Parent tribble {} is notifying child tribble {} of a turn change.", tribbleId,
						child.tribbleId);
				child.nextTurnOccurred();
			}
			moveRandomly();
		}
	}

	@Override
	public void kill() {
		if (child != null) {
			LOG.debug("Parent tribble {} is killing child tribble {}.", tribbleId, child.tribbleId);
			child.kill();
		}
		if (parent != null) {
			parent.removeChild();
		}
		super.kill();
	}

	public Tribble getChild() {
		return child;
	};

	public void removeChild() {
		child = null;
	}

	private BoardCell getRandomVacantNeighbourBoardCell() {
		BoardCell randomVacantNeighbour = null;
		Set<OffsetPair> offsetAttempts = new HashSet<>();

		while (randomVacantNeighbour == null) {
			if (maxAttemptsReached(offsetAttempts.size())) {
				LOG.debug("All cells were occupied. Giving up.");
				break;
			}

			OffsetPair offsetPair = getRandomOffsetPair();

			if (offsetAttempts.contains(offsetPair)) {
				LOG.debug("Offsets {} already attempted. Skipping.", offsetPair);
				continue;
			}
			offsetAttempts.add(offsetPair);

			if (!getLocation().areXAndYOffsetsValid(offsetPair.getXOffset(), offsetPair.getYOffset())) {
				LOG.debug("Offsets {} is not valid. Skipping.", offsetPair);
				continue;
			}

			randomVacantNeighbour = getLocation().getNeighbourByXAndYOffset(offsetPair.getXOffset(),
					offsetPair.getYOffset());
			LOG.debug("Random vacant neighbour board cell to check: randomVacantNeighbour={};", randomVacantNeighbour);

			if (randomVacantNeighbour.isOccupied()) {
				LOG.debug("Board cell was already occupied: occupant={};", randomVacantNeighbour.getOccupant());
				randomVacantNeighbour = null;
			} else {
				LOG.debug("Found a vacant cell. Returning.");
				break;
			}
		}

		return randomVacantNeighbour;
	}

	private void spawnChild() {
		LOG.debug("Tribble {} is spawning a child.", tribbleId);
		BoardCell childLocation = getRandomVacantNeighbourBoardCell();
		if (childLocation != null) {
			child = new Tribble(childLocation, this);
		} else {
			LOG.debug("Couldn't find a valid location to spawn tribble {}'s child.", tribbleId);
		}
	}

	private void moveRandomly() {
		LOG.debug("Tribble {} is making a random move.", tribbleId);
		BoardCell newLocation = getRandomVacantNeighbourBoardCell();
		if (newLocation != null) {
			getLocation().remove(this);
			setLocation(newLocation);
		} else {
			LOG.debug("Couldn't find a valid location to move tribble {}.", tribbleId);
		}
	}

	private boolean maxAttemptsReached(int numberOfAttempts) {
		if (numberOfAttempts == PERIMETER_SIZE) {
			return true;
		}
		return false;
	}

	private OffsetPair getRandomOffsetPair() {
		int randomXOffset = nextInt(0, 2) * (nextBoolean() ? 1 : -1);
		int randomYOffset = nextInt(0, 2) * (nextBoolean() ? 1 : -1);

		OffsetPair offsetPair = new OffsetPair(randomXOffset, randomYOffset);
		LOG.debug("Random offsets generated are: randomXOffset={}; randomYOffset={};", randomXOffset, randomYOffset);

		return offsetPair;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + tribbleId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tribble other = (Tribble) obj;
		if (tribbleId != other.tribbleId)
			return false;
		return true;
	}

	private class OffsetPair {
		int xOffset;
		int yOffset;

		public OffsetPair(int xOffset, int yOffset) {
			super();
			this.xOffset = xOffset;
			this.yOffset = yOffset;
		}

		public int getXOffset() {
			return xOffset;
		}

		public int getYOffset() {
			return yOffset;
		}

		@Override
		public String toString() {
			return String.format("OffsettPair[xOffset=%s yOffset=%s]", xOffset, yOffset);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + xOffset;
			result = prime * result + yOffset;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			OffsetPair other = (OffsetPair) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (xOffset != other.xOffset)
				return false;
			if (yOffset != other.yOffset)
				return false;
			return true;
		}

		private Tribble getOuterType() {
			return Tribble.this;
		}

	}

}
