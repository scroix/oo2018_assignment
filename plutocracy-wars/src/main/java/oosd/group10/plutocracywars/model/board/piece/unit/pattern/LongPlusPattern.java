package oosd.group10.plutocracywars.model.board.piece.unit.pattern;

import java.util.HashSet;
import java.util.Set;

import oosd.group10.plutocracywars.model.board.BoardCell;

public class LongPlusPattern extends AbstractCellPattern {

	private static final long serialVersionUID = -2194491593567976025L;

	public Set<BoardCell> getCellsInPattern(BoardCell currentLocation) {
		Set<BoardCell> cellsInPattern = new HashSet<BoardCell>();

		cellsInPattern.addAll(getCellsOnHorizontalPath(currentLocation));
		cellsInPattern.addAll(getCellsOnVerticalPath(currentLocation));

		return cellsInPattern;
	}

	private Set<BoardCell> getCellsOnHorizontalPath(BoardCell currentLocation) {
		Set<BoardCell> cellsOnHorizontalPath = new HashSet<BoardCell>();
		int currentX = currentLocation.getX();
		int minXOffset = 0 - currentX;
		int maxXOffset = currentLocation.getMaxXCoordinate() - currentX;

		for (int xOffset = minXOffset; xOffset <= maxXOffset; xOffset++) {
			if (offsetResultsInCurrentLocation(xOffset, 0)
					|| offsetResultsInInvalidLocation(currentLocation, xOffset, 0)) {
				continue;
			}
			cellsOnHorizontalPath.add(currentLocation.getNeighbourByXAndYOffset(xOffset, 0));
		}

		return cellsOnHorizontalPath;
	}

	private Set<BoardCell> getCellsOnVerticalPath(BoardCell currentLocation) {
		Set<BoardCell> cellsOnVerticalPath = new HashSet<BoardCell>();
		int currentY = currentLocation.getY();
		int minYOffset = 0 - currentY;
		int maxYOffset = currentLocation.getMaxYCoordinate() - currentY;

		for (int yOffset = minYOffset; yOffset <= maxYOffset; yOffset++) {
			if (offsetResultsInCurrentLocation(0, yOffset)
					|| offsetResultsInInvalidLocation(currentLocation, 0, yOffset)) {
				continue;
			}
			cellsOnVerticalPath.add(currentLocation.getNeighbourByXAndYOffset(0, yOffset));
		}

		return cellsOnVerticalPath;
	}
}
