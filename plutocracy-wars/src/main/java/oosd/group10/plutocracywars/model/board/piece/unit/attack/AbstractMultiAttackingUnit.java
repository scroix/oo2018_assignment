package oosd.group10.plutocracywars.model.board.piece.unit.attack;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import oosd.group10.plutocracywars.model.board.BoardCell;
import oosd.group10.plutocracywars.model.board.piece.TechnologyType;

public abstract class AbstractMultiAttackingUnit extends AbstractAttackingUnit {

	private static final long serialVersionUID = -6698814552933236144L;
	private static final Logger LOG = LogManager.getLogger(AbstractMultiAttackingUnit.class);

	public AbstractMultiAttackingUnit(BoardCell location, TechnologyType technologyType) {
		super(location, technologyType);
	}

	@Override
	public void attack(BoardCell boardCellToAttack) {
		 if (canAttack(boardCellToAttack)) {
			for (BoardCell attackableBoardCell : getBoardCellsCanAttack()) {
				if (attackableBoardCell.getOccupant() != null) {
					LOG.debug("{} attacked {}", this, boardCellToAttack.getOccupant());
					attackableBoardCell.getOccupant().kill();
				}
			}
		 }
		setHasAttacked(true);
		notifyAttackOccurred();
	}

}
