package oosd.group10.plutocracywars.model.board.piece.unit;

public interface UnitActionObservable {

	void attachUnitActionObserver(UnitActionObserver unitActionObserver);

	void detachUnitActionObserver(UnitActionObserver unitActionObserver);

	void notifyUnitActionOccurred();

}
