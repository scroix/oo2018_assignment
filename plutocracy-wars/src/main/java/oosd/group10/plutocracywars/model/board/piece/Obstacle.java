package oosd.group10.plutocracywars.model.board.piece;

import oosd.group10.plutocracywars.model.board.BoardCell;

public class Obstacle extends AbstractBoardPiece {

	private static final long serialVersionUID = 6457340400245913787L;

	public Obstacle(BoardCell location, TechnologyType technologyType) {
		super(location, technologyType);
	}

}
