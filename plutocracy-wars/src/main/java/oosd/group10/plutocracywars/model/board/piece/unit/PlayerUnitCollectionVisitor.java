package oosd.group10.plutocracywars.model.board.piece.unit;

import java.util.HashSet;
import java.util.Set;

import oosd.group10.plutocracywars.model.board.piece.unit.attack.AbstractAttackingUnit;
import oosd.group10.plutocracywars.model.board.piece.unit.attack.Beserker;
import oosd.group10.plutocracywars.model.board.piece.unit.attack.Charger;
import oosd.group10.plutocracywars.model.board.piece.unit.attack.Rifleman;
import oosd.group10.plutocracywars.model.board.piece.unit.attack.Rocketeer;
import oosd.group10.plutocracywars.model.board.piece.unit.attack.SpecOps;

public class PlayerUnitCollectionVisitor implements PlayerUnitVisitor {

	private Set<AbstractAttackingUnit> attackingUnits = new HashSet<>();
	private Set<PlayerUnit> nonAttackingUnits = new HashSet<>();

	public void visit(AbstractAttackingUnit attackingUnit) {
		attackingUnits.add(attackingUnit);
	}

	public Set<AbstractAttackingUnit> getAttackingUnits() {
		return attackingUnits;
	}

	public Set<PlayerUnit> getNonAttackingUnits() {
		return nonAttackingUnits;
	}
	
	@Override
	public void visit(Juggernaut juggernaut) {
		nonAttackingUnits.add(juggernaut);
	}

	@Override
	public void visit(Beserker beserker) {
		attackingUnits.add(beserker);
	}

	@Override
	public void visit(Charger charger) {
		attackingUnits.add(charger);
	}

	@Override
	public void visit(Rifleman rifleman) {
		attackingUnits.add(rifleman);
	}

	@Override
	public void visit(Rocketeer rocketeer) {
		attackingUnits.add(rocketeer);
	}

	@Override
	public void visit(SpecOps specOps) {
		attackingUnits.add(specOps);
	}

}
