package oosd.group10.plutocracywars.model.game;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.SerializationUtils;

import oosd.group10.plutocracywars.model.board.Board;
import oosd.group10.plutocracywars.model.board.BoardImpl;
import oosd.group10.plutocracywars.model.board.piece.Obstacle;

public class GameState implements Serializable, Prototypical<GameState> {

	private static final long serialVersionUID = 5258665955002585713L;

	private Board board = new BoardImpl();
	private List<Player> players = new ArrayList<>();
	private Player currentPlayer;
	private Set<Obstacle> obstacles = new HashSet<>();

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public Player getCurrentPlayer() {
		return currentPlayer;
	}

	public void setCurrentPlayer(Player currentPlayer) {
		this.currentPlayer = currentPlayer;
	}

	public Set<Obstacle> getObstacles() {
		return obstacles;
	}

	public void setObstacles(Set<Obstacle> obstacles) {
		this.obstacles = obstacles;
	}

	public void addObstacle(Obstacle obstacle) {
		obstacles.add(obstacle);
	}

	public void flush(Game game) {
		for (Obstacle obstacle : obstacles) {
			if (obstacle instanceof TurnObserver) {
				game.detachTurnObserver((TurnObserver) obstacle);
			}
		}
	}

	public void reattach(Game game) {
		for (Player player : players) {
			player.reattach(game);
		}
		for (Obstacle obstacle : obstacles) {
			if (obstacle instanceof TurnObserver) {
				game.attachTurnObserver((TurnObserver) obstacle);
			}
		}
	}

	@Override
	public GameState deepClone() {
		return SerializationUtils.clone(this);
	}

}
