package oosd.group10.plutocracywars.model.board.piece;

import java.io.Serializable;
import java.util.Set;

import oosd.group10.plutocracywars.model.board.BoardCell;

public interface BoardPiece extends Serializable {

	/**
	 * @return The piece's current location on the board.
	 */
	BoardCell getLocation();

	/**
	 * @return The technology type of the piece or null if untyped.
	 */
	TechnologyType getTechnologyType();

	/**
	 * @param technologyType
	 * @return True if the given technology type defeats the piece's technology type, or true if the piece is untyped. Otherwise false.
	 */
	boolean isDefeatedBy(TechnologyType technologyType);

	/**
	 * @return The set of all technology types that can defeat this piece. If the piece is untyped, it will contain the set of all technology types.
	 */
	Set<TechnologyType> getDefeatedBy();

	/**
	 * Kill the unit and remove it from the board.
	 */
	void kill();

}
