package oosd.group10.plutocracywars.model.board.piece.unit;

import oosd.group10.plutocracywars.model.board.piece.BoardPiece;
import oosd.group10.plutocracywars.model.game.Player;

public interface PlayerUnit extends BoardPiece, Moveable, UnitActionObservable {

	Player getOwner();

	void setOwner(Player owner);

	PlayerUnitType getPlayerUnitType();

	void accept(PlayerUnitVisitor playerUnitVisitor);

}
