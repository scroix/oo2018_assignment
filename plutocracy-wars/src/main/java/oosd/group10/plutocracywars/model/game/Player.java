package oosd.group10.plutocracywars.model.game;

import java.io.Serializable;
import java.util.Set;

import oosd.group10.plutocracywars.model.board.piece.unit.Moveable;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnit;
import oosd.group10.plutocracywars.model.board.piece.unit.attack.Attacker;

public interface Player extends Serializable {

	Set<PlayerUnit> getUnits();

	Set<Attacker> getUnitsAbleToAttack();

	Set<Moveable> getUnitsAbleToMove();

	void addUnit(PlayerUnit unit);

	void removeUnit(PlayerUnit unit);

	String getName();

	void reattach(Game game);

}
