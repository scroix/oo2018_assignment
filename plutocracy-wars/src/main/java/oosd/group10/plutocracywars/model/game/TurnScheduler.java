package oosd.group10.plutocracywars.model.game;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

public class TurnScheduler {

	private ScheduledExecutorService turnScheduler = Executors.newScheduledThreadPool(1);
	private ScheduledFuture<?> forceNextTurnHandle;
	
	public TurnScheduler(Runnable forceNextTurn, int turnTimeInSeconds) {
		forceNextTurnHandle = turnScheduler.schedule(forceNextTurn, turnTimeInSeconds, SECONDS);
	}

	public void cancelScheduledNextTurn() {
		forceNextTurnHandle.cancel(true);
	}

}
