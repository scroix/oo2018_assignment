package oosd.group10.plutocracywars.model.board.piece.unit.attack;

import static oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitType.CHARGER;

import oosd.group10.plutocracywars.model.board.BoardCell;
import oosd.group10.plutocracywars.model.board.piece.TechnologyType;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitType;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitVisitor;
import oosd.group10.plutocracywars.model.board.piece.unit.pattern.BlockableLongPlusPattern;
import oosd.group10.plutocracywars.model.board.piece.unit.pattern.CellPattern;
import oosd.group10.plutocracywars.model.board.piece.unit.pattern.SquarePattern;

public class Charger extends AbstractAttackingUnit {

	private static final long serialVersionUID = -7534860871611942429L;
	private static final CellPattern ATTACK_PATTERN = new SquarePattern();
	private static final CellPattern MOVEMENT_PATTERN = new BlockableLongPlusPattern();
	private static final PlayerUnitType PLAYER_UNIT_TYPE = CHARGER;
	
	public Charger(BoardCell location, TechnologyType technologyType) {
		super(location, technologyType);
	}

	@Override
	public CellPattern getAttackPattern() {
		return ATTACK_PATTERN;
	}

	@Override
	public CellPattern getMovementPattern() {
		return MOVEMENT_PATTERN;
	}

	@Override
	public PlayerUnitType getPlayerUnitType() {
		return PLAYER_UNIT_TYPE;
	}

	@Override
	public void accept(PlayerUnitVisitor playerUnitVisitor) {
		playerUnitVisitor.visit(this);
	}
}
