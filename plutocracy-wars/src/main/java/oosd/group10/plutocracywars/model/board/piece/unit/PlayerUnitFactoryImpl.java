package oosd.group10.plutocracywars.model.board.piece.unit;

import static java.lang.String.format;

import oosd.group10.plutocracywars.model.board.BoardCell;
import oosd.group10.plutocracywars.model.board.piece.TechnologyType;
import oosd.group10.plutocracywars.model.board.piece.unit.attack.Beserker;
import oosd.group10.plutocracywars.model.board.piece.unit.attack.Charger;
import oosd.group10.plutocracywars.model.board.piece.unit.attack.Rifleman;
import oosd.group10.plutocracywars.model.board.piece.unit.attack.Rocketeer;
import oosd.group10.plutocracywars.model.board.piece.unit.attack.SpecOps;

public class PlayerUnitFactoryImpl implements PlayerUnitFactory {

	@Override
	public PlayerUnit createPlayerUnit(BoardCell location, PlayerUnitType playerUnitType,
			TechnologyType technologyType) {
		PlayerUnit playerUnit;

		switch (playerUnitType) {
		case RIFLEMAN:
			playerUnit = new Rifleman(location, technologyType);
			break;
		case BESERKER:
			playerUnit = new Beserker(location, technologyType);
			break;
		case ROCKETEER:
			playerUnit = new Rocketeer(location, technologyType);
			break;
		case CHARGER:
			playerUnit = new Charger(location, technologyType);
			break;
		case JUGGERNAUT:
			playerUnit = new Juggernaut(location, technologyType);
			break;
		case SPECOPS:
			playerUnit = new SpecOps(location, technologyType);
			break;
		default:
			throw new IllegalStateException(
					format("This PlayerUnitFactory has no implementation for the requested PlayerUnitType: "
							+ "className=%s; playerUnitType=%s;", this.getClass().getName(), playerUnitType));
		}

		return playerUnit;
	}

}
