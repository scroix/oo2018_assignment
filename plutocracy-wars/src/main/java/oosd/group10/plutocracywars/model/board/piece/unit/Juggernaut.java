package oosd.group10.plutocracywars.model.board.piece.unit;

import static oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitType.JUGGERNAUT;

import java.util.Collections;
import java.util.Set;

import oosd.group10.plutocracywars.model.board.BoardCell;
import oosd.group10.plutocracywars.model.board.piece.TechnologyType;
import oosd.group10.plutocracywars.model.board.piece.unit.pattern.BlockableLongPlusPattern;
import oosd.group10.plutocracywars.model.board.piece.unit.pattern.CellPattern;

public class Juggernaut extends AbstractPlayerUnit {

	private static final long serialVersionUID = -4700641596889605387L;
	private static final CellPattern MOVEMENT_PATTERN = new BlockableLongPlusPattern();
	private static final PlayerUnitType PLAYER_UNIT_TYPE = JUGGERNAUT;

	private final TechnologyType soleWeakness;

	public Juggernaut(BoardCell location, TechnologyType technologyType) {
		super(location, technologyType);
		soleWeakness = getRandomSoleWeakness();
	}

	@Override
	public CellPattern getMovementPattern() {
		return MOVEMENT_PATTERN;
	}

	@Override
	public PlayerUnitType getPlayerUnitType() {
		return PLAYER_UNIT_TYPE;
	}

	@Override
	public boolean isDefeatedBy(TechnologyType technologyType) {
		return technologyType == soleWeakness;
	}

	@Override
	public Set<TechnologyType> getDefeatedBy() {
		return Collections.singleton(soleWeakness);
	}
	
	@Override
	public void accept(PlayerUnitVisitor playerUnitVisitor) {
		playerUnitVisitor.visit(this);
	}

	private TechnologyType getRandomSoleWeakness() {
		Set<TechnologyType> defeatedBy = getTechnologyType().getDefeatedBy();
		return defeatedBy.stream().skip((int) (defeatedBy.size() * Math.random())).findFirst().get();
	}

}
