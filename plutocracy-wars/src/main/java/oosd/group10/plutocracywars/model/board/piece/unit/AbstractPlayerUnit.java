package oosd.group10.plutocracywars.model.board.piece.unit;

import static java.lang.String.format;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import oosd.group10.plutocracywars.model.board.BoardCell;
import oosd.group10.plutocracywars.model.board.piece.AbstractBoardPiece;
import oosd.group10.plutocracywars.model.board.piece.TechnologyType;
import oosd.group10.plutocracywars.model.board.piece.unit.pattern.CellPattern;
import oosd.group10.plutocracywars.model.game.Player;

public abstract class AbstractPlayerUnit extends AbstractBoardPiece implements PlayerUnit {

	private static final long serialVersionUID = 165501814090670443L;
	private static final Logger LOG = LogManager.getLogger(AbstractPlayerUnit.class);

	private Player owner;
	private boolean hasMoved;

	private transient List<UnitActionObserver> unitActionObservers;

	protected AbstractPlayerUnit(BoardCell location, TechnologyType technologyType) {
		super(location, technologyType);
	}

	public Player getOwner() {
		return owner;
	}

	public void setOwner(Player owner) {
		this.owner = owner;
	}

	public abstract CellPattern getMovementPattern();

	public void moveTo(BoardCell boardCell) {
		throwExceptionIfNotMoveable(boardCell);

		LOG.debug("{} moved from {} to {}", this, boardCell, getLocation());

		getLocation().remove(this);
		setLocation(boardCell);
		setHasMoved(true);

		notifyMovedOccurred();
	}

	public boolean canMoveTo(BoardCell boardCell) {
		return !hasMoved && getBoardCellsCanMoveTo().contains(boardCell);
	}

	@Override
	public boolean hasMoved() {
		return hasMoved;
	}

	public void setHasMoved(boolean hasMoved) {
		this.hasMoved = hasMoved;
	}

	@Override
	public void kill() {
		super.kill();
		if (owner != null) {
			owner.removeUnit(this);
		}
	}

	@Override
	public Set<BoardCell> getBoardCellsCanMoveTo() {
		if (hasMoved) {
			return Collections.emptySet();
		}
		return getMovementPattern().getCellsInPattern(getLocation()).stream()
				.filter(boardCell -> !boardCell.isOccupied()).collect(Collectors.toSet());
	}

	public void resetForNextRound() {
		setHasMoved(false);
	}

	public abstract PlayerUnitType getPlayerUnitType();

	@Override
	public void attachUnitActionObserver(UnitActionObserver unitActionObserver) {
		if (unitActionObservers == null) {
			unitActionObservers = new ArrayList<UnitActionObserver>();
		}
		LOG.debug("Attaching unit action observer for playerUnit: unitActionObserver={}; playerUnit={};",
				unitActionObserver, this);
		unitActionObservers.add(unitActionObserver);
	}

	@Override
	public void detachUnitActionObserver(UnitActionObserver unitActionObserver) {
		if (unitActionObservers == null) {
			return;
		}
		LOG.debug("Removing unit action observer for playerUnit: unitActionObserver={}; playerUnit={};",
				unitActionObserver, this);
		unitActionObservers.remove(unitActionObserver);
	}

	@Override
	public void notifyUnitActionOccurred() {
		if (unitActionObservers != null) {
			for (UnitActionObserver unitActionObserver : unitActionObservers) {
				LOG.debug("Notifying unit action observer for playerUnit: unitActionObserver={}; playerUnit={};",
						unitActionObserver, this);
				unitActionObserver.unitActionOccurred(this);
			}
		}
	}
	
	protected void notifyMovedOccurred() {
		notifyUnitActionOccurred();
	}

	private void throwExceptionIfNotMoveable(BoardCell boardCell) {
		if (!getBoardCellsCanMoveTo().contains(boardCell)) {
			throw new IllegalArgumentException(
					format("Attempted to move to an invalid location: playerUnit=%s; currentLocation=%s; newLocation=$s;",
							this, getLocation(), boardCell));
		}
	}
}
