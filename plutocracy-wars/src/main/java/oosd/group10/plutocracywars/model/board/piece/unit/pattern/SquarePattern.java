package oosd.group10.plutocracywars.model.board.piece.unit.pattern;

import java.util.HashSet;
import java.util.Set;

import oosd.group10.plutocracywars.model.board.BoardCell;

public class SquarePattern extends AbstractCellPattern {

	private static final long serialVersionUID = 6643126765505307221L;

	public Set<BoardCell> getCellsInPattern(BoardCell currentLocation) {
		Set<BoardCell> cellsInPattern = new HashSet<BoardCell>();

		for (int xOffset = -1; xOffset <= 1; xOffset++) {
			for (int yOffset = -1; yOffset <= 1; yOffset++) {
				if (offsetResultsInCurrentLocation(xOffset, yOffset)
						|| offsetResultsInInvalidLocation(currentLocation, xOffset, yOffset)) {
					continue;
				}
				cellsInPattern.add(currentLocation.getNeighbourByXAndYOffset(xOffset, yOffset));
			}
		}

		return cellsInPattern;
	}

}
