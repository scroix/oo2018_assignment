package oosd.group10.plutocracywars.model.game;

public class TurnViolationException extends RuntimeException {

	private static final long serialVersionUID = -3298244951999899937L;

	public TurnViolationException() {
	}

	public TurnViolationException(String message) {
		super(message);
	}

}
