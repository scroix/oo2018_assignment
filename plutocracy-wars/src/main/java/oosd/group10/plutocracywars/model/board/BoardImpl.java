package oosd.group10.plutocracywars.model.board;

import static java.lang.String.format;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BoardImpl implements Board {

	public static final int DEFAULT_BOARD_COLUMNS = 12;
	public static final int DEFAULT_BOARD_ROWS = 12;

	private static final long serialVersionUID = -5988482883773146885L;

	private final int columns;
	private final int rows;
	private Map<CellKey, BoardCell> cells;

	public BoardImpl() {
		this(DEFAULT_BOARD_COLUMNS, DEFAULT_BOARD_ROWS);
	}

	public BoardImpl(int columns, int rows) {
		this.columns = columns;
		this.rows = rows;
		initialiseCells(columns, rows);
	}

	public int getColumns() {
		return columns;
	}

	public int getRows() {
		return rows;
	};

	public boolean areXAndYCoordinatesValid(int x, int y) {
		return isXCoordinateValid(x) && isYCoordinateValid(y);
	}

	public boolean isXCoordinateValid(int x) {
		return x >= 0 && x < columns;
	}

	public boolean isYCoordinateValid(int y) {
		return y >= 0 && y < rows;
	}

	public BoardCell getBoardCellByXAndYCoordinates(int x, int y) {
		throwExceptionIfXAndYCoordinatesAreInvalid(x, y);
		return cells.get(new CellKey(x, y));
	}

	private void throwExceptionIfXAndYCoordinatesAreInvalid(int x, int y) {
		if (!areXAndYCoordinatesValid(x, y)) {
			throw new IllegalArgumentException(
					format("Getting neighbour by provided coordinates is out of bounds: x=%s; y=%s;", x, y));
		}
	}

	private void initialiseCells(int columns, int rows) {
		cells = new HashMap<CellKey, BoardCell>();
		for (int x = 0; x < columns; x++) {
			for (int y = 0; y < rows; y++) {
				BoardCellImpl cell = new BoardCellImpl(x, y);
				cell.setBoard(this);
				cells.put(new CellKey(x, y), cell);
			}
		}
	}

	@Override
	public List<BoardCell> getBoardCells() {
		List<BoardCell> boardCells = new ArrayList<>(cells.size());

		for (int x = 0; x < columns; x++) {
			for (int y = 0; y < rows; y++) {
				boardCells.add(getBoardCellByXAndYCoordinates(x, y));
			}
		}

		return boardCells;
	}

}
