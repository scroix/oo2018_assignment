package oosd.group10.plutocracywars.model.board.piece.unit.attack;

import static oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitType.SPECOPS;

import oosd.group10.plutocracywars.model.board.BoardCell;
import oosd.group10.plutocracywars.model.board.piece.TechnologyType;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitType;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitVisitor;
import oosd.group10.plutocracywars.model.board.piece.unit.pattern.CellPattern;
import oosd.group10.plutocracywars.model.board.piece.unit.pattern.PlusPattern;
import oosd.group10.plutocracywars.model.board.piece.unit.pattern.SparseSquarePattern;

public class SpecOps extends AbstractMultiAttackingUnit {

	private static final long serialVersionUID = -6437763458776189619L;
	private static final CellPattern ATTACK_PATTERN = new PlusPattern();
	private static final CellPattern MOVEMENT_PATTERN = new SparseSquarePattern();
	private static final PlayerUnitType PLAYER_UNIT_TYPE = SPECOPS;

	public SpecOps(BoardCell location, TechnologyType technologyType) {
		super(location, technologyType);
	}

	@Override
	public CellPattern getAttackPattern() {
		return ATTACK_PATTERN;
	}

	@Override
	public CellPattern getMovementPattern() {
		return MOVEMENT_PATTERN;
	}

	@Override
	public PlayerUnitType getPlayerUnitType() {
		return PLAYER_UNIT_TYPE;
	}

	@Override
	public void accept(PlayerUnitVisitor playerUnitVisitor) {
		playerUnitVisitor.visit(this);
	}

}
