package oosd.group10.plutocracywars.model.board;

import static java.lang.String.format;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import oosd.group10.plutocracywars.model.board.piece.BoardPiece;

public class BoardCellImpl implements BoardCell {

	private static final long serialVersionUID = -1027357508525055755L;

	private final int x;
	private final int y;
	private BoardPiece occupant;
	private Board board;

	public BoardCellImpl(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public boolean isOccupied() {
		return occupant != null;
	}

	public void receive(BoardPiece boardPiece) {
		throwExceptionIfCannotReceive(boardPiece);
		occupant = boardPiece;
	}

	public void remove(BoardPiece boardPiece) {
		throwExceptionIfNotOccupant(boardPiece);
		occupant = null;
	}

	public boolean contains(BoardPiece boardPiece) {
		if (occupant == null) {
			return false;
		}
		return occupant.equals(boardPiece);
	}

	public BoardPiece getOccupant() {
		return occupant;
	}

	public int getMaxXCoordinate() {
		return board.getColumns() - 1;
	}

	public int getMaxYCoordinate() {
		return board.getRows() - 1;
	}

	public BoardCell getNeighbourByXAndYOffset(int xOffset, int yOffset) {
		return board.getBoardCellByXAndYCoordinates(x + xOffset, y + yOffset);
	}

	@Override
	public boolean areXAndYOffsetsValid(int xOffset, int yOffset) {
		return board.areXAndYCoordinatesValid(x + xOffset, y + yOffset);
	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toStringExclude(this, "occupant", "board");
	}

	private void throwExceptionIfCannotReceive(BoardPiece boardPiece) {
		if (isOccupied()) {
			throw new IllegalArgumentException(
					format("Board cell is already occupied: boardCell=%s; violatingBoardPiece=%s;", this, boardPiece));
		}
	}

	private void throwExceptionIfNotOccupant(BoardPiece boardPiece) {
		if (!contains(boardPiece)) {
			throw new IllegalArgumentException(format(
					"Board piece does not occupy this cell: boardCell=%s; violatingBoardPiece=%s;", this, boardPiece));
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BoardCellImpl other = (BoardCellImpl) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
}
