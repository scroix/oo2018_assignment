package oosd.group10.plutocracywars.model.board.piece.unit.pattern;

import java.util.HashSet;
import java.util.Set;

import oosd.group10.plutocracywars.model.board.BoardCell;

public class SparseSquarePattern extends AbstractCellPattern {

	private static final long serialVersionUID = -2709786184148414468L;

	public Set<BoardCell> getCellsInPattern(BoardCell currentLocation) {
		Set<BoardCell> cellsInPattern = new HashSet<BoardCell>();

		for (int xOffset = -2; xOffset <= 2; xOffset += 2) {
			for (int yOffset = -2; yOffset <= 2; yOffset += 2) {
				if (offsetResultsInCurrentLocation(xOffset, yOffset)
						|| offsetResultsInInvalidLocation(currentLocation, xOffset, yOffset)) {
					continue;
				}
				cellsInPattern.add(currentLocation.getNeighbourByXAndYOffset(xOffset, yOffset));
			}
		}

		return cellsInPattern;
	}

}
