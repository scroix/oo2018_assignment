package oosd.group10.plutocracywars.model.board.piece.unit.pattern;

import java.util.Set;

import oosd.group10.plutocracywars.model.board.BoardCell;

public interface CellPattern {
	public Set<BoardCell> getCellsInPattern(BoardCell currentLocation);
}
