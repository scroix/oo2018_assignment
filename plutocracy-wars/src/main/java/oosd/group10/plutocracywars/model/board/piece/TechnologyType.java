package oosd.group10.plutocracywars.model.board.piece;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public enum TechnologyType {
	PLASMA, EXPLOSIVE, NANOTECH, DARK_MATTER, GRAVITY;

	public boolean canDefeat(TechnologyType otherTechnologyType) {
		boolean canDefeat = false;
		switch (this) {
		case PLASMA:
			canDefeat = otherTechnologyType == EXPLOSIVE || otherTechnologyType == DARK_MATTER;
			break;
		case EXPLOSIVE:
			canDefeat = otherTechnologyType == NANOTECH || otherTechnologyType == GRAVITY;
			break;
		case NANOTECH:
			canDefeat = otherTechnologyType == DARK_MATTER || otherTechnologyType == PLASMA;
			break;
		case DARK_MATTER:
			canDefeat = otherTechnologyType == GRAVITY || otherTechnologyType == EXPLOSIVE;
			break;
		case GRAVITY:
			canDefeat = otherTechnologyType == PLASMA || otherTechnologyType == NANOTECH;
			break;
		default:
			throw new IllegalStateException(String.format(
					"The technology type win/lose conditions have not been configured: thisTechnologyType=%s; otherTechnologyType=%s;",
					this, otherTechnologyType));
		}
		return canDefeat;
	}

	public boolean isDefeatedBy(TechnologyType otherTechnologyType) {
		return otherTechnologyType.canDefeat(this);
	}

	public Set<TechnologyType> getDefeats() {
		Set<TechnologyType> defeats = new HashSet<TechnologyType>();

		for (TechnologyType otherTechnologyType : TechnologyType.values()) {
			if (this.canDefeat(otherTechnologyType)) {
				defeats.add(otherTechnologyType);
			}
		}

		return Collections.unmodifiableSet(defeats);
	}

	public Set<TechnologyType> getDefeatedBy() {
		Set<TechnologyType> defeatedBy = new HashSet<TechnologyType>();

		for (TechnologyType otherTechnologyType : TechnologyType.values()) {
			if (otherTechnologyType.canDefeat(this)) {
				defeatedBy.add(otherTechnologyType);
			}
		}

		return Collections.unmodifiableSet(defeatedBy);
	}
}
