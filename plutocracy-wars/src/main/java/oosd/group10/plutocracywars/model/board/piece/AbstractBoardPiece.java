package oosd.group10.plutocracywars.model.board.piece;

import java.util.Set;

import oosd.group10.plutocracywars.model.board.BoardCell;

public abstract class AbstractBoardPiece implements BoardPiece {

	private static final long serialVersionUID = -4436864809133072096L;

	private BoardCell location;
	private TechnologyType technologyType;

	public AbstractBoardPiece(BoardCell location, TechnologyType technologyType) {
		super();
		setLocation(location);
		this.technologyType = technologyType;
	}

	public BoardCell getLocation() {
		return location;
	}

	public void setLocation(BoardCell location) {
		this.location = location;
		location.receive(this);
	}

	public TechnologyType getTechnologyType() {
		return technologyType;
	}

	public void setTechnologyType(TechnologyType technologyType) {
		this.technologyType = technologyType;
	}

	public boolean isDefeatedBy(TechnologyType technologyType) {
		return technologyType.canDefeat(this.technologyType);
	}

	public Set<TechnologyType> getDefeatedBy() {
		return technologyType.getDefeatedBy();
	}

	public void kill() {
		location.remove(this);
		location = null;
	}
}
