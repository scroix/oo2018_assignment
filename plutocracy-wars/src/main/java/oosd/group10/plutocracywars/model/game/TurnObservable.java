package oosd.group10.plutocracywars.model.game;

public interface TurnObservable {

	void attachTurnObserver(TurnObserver turnObserver);

	void detachTurnObserver(TurnObserver turnObserver);

	void notifyNextTurnOccurred();

}
