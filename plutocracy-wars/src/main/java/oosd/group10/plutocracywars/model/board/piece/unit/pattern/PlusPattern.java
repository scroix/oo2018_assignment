package oosd.group10.plutocracywars.model.board.piece.unit.pattern;

import java.util.HashSet;
import java.util.Set;

import oosd.group10.plutocracywars.model.board.BoardCell;

public class PlusPattern extends AbstractCellPattern {

	private static final long serialVersionUID = -5922651649686549849L;

	public Set<BoardCell> getCellsInPattern(BoardCell currentLocation) {
		Set<BoardCell> boardCellsCanReachByWalking = new HashSet<BoardCell>();

		for (int xOffset = -1; xOffset <= 1; xOffset++) {
			if (offsetResultsInCurrentLocation(xOffset, 0)
					|| offsetResultsInInvalidLocation(currentLocation, xOffset, 0)) {
				continue;
			}
			boardCellsCanReachByWalking.add(currentLocation.getNeighbourByXAndYOffset(xOffset, 0));
		}

		for (int yOffset = -1; yOffset <= 1; yOffset++) {
			if (offsetResultsInCurrentLocation(0, yOffset)
					|| offsetResultsInInvalidLocation(currentLocation, 0, yOffset)) {
				continue;
			}
			boardCellsCanReachByWalking.add(currentLocation.getNeighbourByXAndYOffset(0, yOffset));
		}

		return boardCellsCanReachByWalking;
	}

}
