package oosd.group10.plutocracywars.model.board.piece.unit.attack;

import static oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitType.BESERKER;

import oosd.group10.plutocracywars.model.board.BoardCell;
import oosd.group10.plutocracywars.model.board.piece.TechnologyType;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitType;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitVisitor;
import oosd.group10.plutocracywars.model.board.piece.unit.pattern.CellPattern;
import oosd.group10.plutocracywars.model.board.piece.unit.pattern.PlusPattern;
import oosd.group10.plutocracywars.model.board.piece.unit.pattern.SquarePattern;

public class Beserker extends AbstractMultiAttackingUnit {

	private static final long serialVersionUID = -9168840316028132360L;
	private static final CellPattern ATTACK_PATTERN = new PlusPattern();
	private static final CellPattern MOVEMENT_PATTERN = new SquarePattern();
	private static final PlayerUnitType PLAYER_UNIT_TYPE = BESERKER;

	public Beserker(BoardCell location, TechnologyType technologyType) {
		super(location, technologyType);
	}

	@Override
	public CellPattern getAttackPattern() {
		return ATTACK_PATTERN;
	}

	@Override
	public CellPattern getMovementPattern() {
		return MOVEMENT_PATTERN;
	}

	@Override
	public PlayerUnitType getPlayerUnitType() {
		return PLAYER_UNIT_TYPE;
	}

	@Override
	public void accept(PlayerUnitVisitor playerUnitVisitor) {
		playerUnitVisitor.visit(this);
	}

}
