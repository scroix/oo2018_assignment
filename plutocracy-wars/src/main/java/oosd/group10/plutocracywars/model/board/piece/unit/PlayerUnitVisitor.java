package oosd.group10.plutocracywars.model.board.piece.unit;

import oosd.group10.plutocracywars.model.board.piece.unit.attack.Beserker;
import oosd.group10.plutocracywars.model.board.piece.unit.attack.Charger;
import oosd.group10.plutocracywars.model.board.piece.unit.attack.Rifleman;
import oosd.group10.plutocracywars.model.board.piece.unit.attack.Rocketeer;
import oosd.group10.plutocracywars.model.board.piece.unit.attack.SpecOps;

public interface PlayerUnitVisitor {

	void visit(Juggernaut juggernaut);

	void visit(Beserker beserker);

	void visit(Charger charger);

	void visit(Rifleman rifleman);

	void visit(Rocketeer rocketeer);

	void visit(SpecOps specOps);

}