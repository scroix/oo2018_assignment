package oosd.group10.plutocracywars.model.board.piece.unit;

public interface UnitActionObserver {

	void unitActionOccurred(PlayerUnit playerUnit);

}
