package oosd.group10.plutocracywars;

import static org.apache.commons.lang3.RandomUtils.nextBoolean;
import static org.apache.commons.lang3.RandomUtils.nextInt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import oosd.group10.plutocracywars.model.board.Board;
import oosd.group10.plutocracywars.model.board.BoardCell;
import oosd.group10.plutocracywars.model.board.CellKey;
import oosd.group10.plutocracywars.model.board.piece.BoardPiece;
import oosd.group10.plutocracywars.model.board.piece.Obstacle;
import oosd.group10.plutocracywars.model.board.piece.TechnologyType;
import oosd.group10.plutocracywars.model.board.piece.Tribble;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnit;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitFactory;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitFactoryImpl;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnitType;
import oosd.group10.plutocracywars.model.game.Game;
import oosd.group10.plutocracywars.model.game.Player;

public class BattleInitaliser {

	public final String[] PLAYER_NAMES = { "Brock Fabrication Machines", "Chinese Strategic",
			"European Imperial Service", "Genetic Microco", "German Media Networks", "Japanese Training Integration",
			"Paragon Genetibank", "Moreno Physiosystems", "Terrell Gas Insurance" };
	
	private int tribbleCount = 1;
	private int playerUnitCount = 3;
	public static final int PLAYER_COUNT = 2;

    private Board board;
    private Game game;
    
    private List<PlayerUnitType> uniqueTypes;
    private List<TechnologyType> uniqueTechnologies;

	/*
	 * Initialises a fresh game board with player units.
	 */
	public BattleInitaliser(Game game) {

		playerUnitCount = game.getNumberOfUnitsPerPlayer();
		this.game = game;
		this.board = game.getBoard();
		
		// Maintain a list of unused types and technologies as we collect our unique units for the board
		uniqueTypes = new ArrayList<PlayerUnitType>(Arrays.asList(PlayerUnitType.values()));
		uniqueTechnologies = new ArrayList<TechnologyType>(Arrays.asList(TechnologyType.values()));

		initalisePlayerUnits();
		initaliseObstacles();
		initaliseTribbles();
	}
	
	/*
	 * Provides game players with their starting units.
	 */
	private void initalisePlayerUnits() {

		List<Player> players = game.getPlayers();

		int playerNum = 1;
		for (Player player : players) {

			while (player.getUnits().size() < playerUnitCount) {

				PlayerUnit unit = getUniqueUnit(player, playerNum);
				player.addUnit(unit);
			}
			playerNum++;
		}

	}
	
	/*
	 * Creates obstacles.
	 */
	private void initaliseObstacles() {
		
		Obstacle o1 = new Obstacle(getRandomVacantBoardCell(), TechnologyType.DARK_MATTER);
		Obstacle o2 = new Obstacle(getRandomVacantBoardCell(), TechnologyType.EXPLOSIVE);
		Obstacle o3 = new Obstacle(getRandomVacantBoardCell(), TechnologyType.GRAVITY);
		Obstacle o4 = new Obstacle(getRandomVacantBoardCell(), TechnologyType.NANOTECH);
		Obstacle o5 = new Obstacle(getRandomVacantBoardCell(), TechnologyType.PLASMA);
		
		game.addObstacle(o1);
		game.addObstacle(o2);
		game.addObstacle(o3);
		game.addObstacle(o4);
		game.addObstacle(o5);
	}
	
	/*
	 * Creates tribbles.
	 */
	private void initaliseTribbles() {
		
		for (int x = 0; x < tribbleCount; x++) {
			game.addObstacle(new Tribble(getRandomVacantBoardCell()));
		}
	}
	
	/*
	 * Attempts to provide a unique PlayerUnit based on a list of previously
	 * utilised types.
	 */
	private PlayerUnit getUniqueUnit(Player player, int playerNum) {

		Random rand = new Random();

		PlayerUnitFactory factory = new PlayerUnitFactoryImpl();

		// Location
		BoardCell location = getUniqueLocation(playerNum, rand);
		// Type
		PlayerUnitType type = getUniqueAttributeType(rand, uniqueTypes, PlayerUnitType.values());
		// Technology
		TechnologyType technology = getUniqueAttributeType(rand, uniqueTechnologies, TechnologyType.values());

		BoardPiece boardPiece = factory.createPlayerUnit(location, type, technology);

		return (PlayerUnit) boardPiece;
	}
	
	/*
	 * Supports maximum two players. Offering a random location for their unit based
	 * on player ordering.
	 */
	private BoardCell getUniqueLocation(int playerNum, Random rand) {

		BoardCell location = null;

		int row = -1;
		switch (playerNum) {
		case 1:
			row = board.getRows() - 1;
			break;
		case 2:
			row = 0;
			break;
		default:
			row = -1;
			break;
		}

		int column;
		while (location == null) {

			column = rand.nextInt(board.getColumns() - 1);

			location = board.getBoardCellByXAndYCoordinates(column, row);
			if (location.isOccupied()) {
				location = null;
			}
		}

		return location;

	}
	
	/*
	 * Generic method to provide either a PlayerUnitType or TechnologyType. In both
	 * cases, first exhausting all unique options before allowing duplications.
	 */
	private <T> T getUniqueAttributeType(Random rand, List<T> uniqueAttributes, T[] types) {

		T candidateType = null;

		int selection;
		if (!uniqueAttributes.isEmpty()) {
			selection = (uniqueAttributes.size() > 1) ? rand.nextInt(uniqueAttributes.size() - 1) : 0;
			candidateType = uniqueAttributes.get(selection);
			uniqueAttributes.remove(selection);
		} else {
			candidateType = types[rand.nextInt(types.length - 1)];
		}

		return candidateType;
	}
	
	
	
	/*
	 * Returns a hopefully initialised instance of the game.
	 */
	public Game getSetupBattle() {
		return game;
	}
	
	private BoardCell getRandomVacantBoardCell() {
		BoardCell randomVacantNeighbour = null;

		while (randomVacantNeighbour == null) {
			int randomX = nextInt(0, board.getColumns());
			int randomY = nextInt(0, board.getRows());

			randomVacantNeighbour = board.getBoardCellByXAndYCoordinates(randomX, randomY);

			if (randomVacantNeighbour.isOccupied()) {
				randomVacantNeighbour = null;
			} else {
				break;
			}
		}

		return randomVacantNeighbour;
	}

}