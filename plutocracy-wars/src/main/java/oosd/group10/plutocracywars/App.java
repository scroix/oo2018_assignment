package oosd.group10.plutocracywars;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import oosd.group10.plutocracywars.controller.GameSceneController;
import oosd.group10.plutocracywars.controller.scene.BattleSceneController;
import oosd.group10.plutocracywars.controller.scene.MainMenuSceneController;
import oosd.group10.plutocracywars.model.game.Game;
import oosd.group10.plutocracywars.model.game.GameImpl;

public class App extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// Create a Main Menu scene(FX) VIEW
		Scene scene = new Scene(new MainMenuSceneController().getRootPane());
		primaryStage.setTitle("Plutocracy Wars");
		primaryStage.setScene(scene);
		primaryStage.setResizable(false);
		primaryStage.show();
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
		    @Override
		    public void handle(WindowEvent t) {
		        Platform.exit();
		        System.exit(0);
		    }
		});
		GameSceneController.getInstance().setScene(scene);
	}
}
