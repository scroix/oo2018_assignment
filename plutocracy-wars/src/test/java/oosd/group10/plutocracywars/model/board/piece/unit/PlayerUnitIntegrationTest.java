package oosd.group10.plutocracywars.model.board.piece.unit;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import oosd.group10.plutocracywars.model.board.Board;
import oosd.group10.plutocracywars.model.board.BoardCell;
import oosd.group10.plutocracywars.model.board.BoardImpl;
import oosd.group10.plutocracywars.model.board.piece.TechnologyType;
import oosd.group10.plutocracywars.model.board.piece.unit.pattern.BlockableLongPlusPattern;
import oosd.group10.plutocracywars.model.board.piece.unit.pattern.CellPattern;
import oosd.group10.plutocracywars.model.board.piece.unit.pattern.SparseSquarePattern;
import oosd.group10.plutocracywars.model.board.piece.unit.pattern.SquarePattern;

public class PlayerUnitIntegrationTest {

	private Board board;
	private PlayerUnit mover;
	BoardCell currentLocation;
	BoardCell newLocation;

	@Before
	public void setUp() {
		board = new BoardImpl();
		currentLocation = getMiddleCellFromBoard();
	}

	@After
	public void tearDown() {
		board = null;
		mover = null;
		currentLocation = null;
		newLocation = null;
	}

	@Test
	public void whenWalkingToValidLocationThenMoveSuccessfully() {
		newLocation = currentLocation.getNeighbourByXAndYOffset(1, 1);
		mover = new Mover(currentLocation, new SquarePattern());

		performMoveAndAssertions();
	}

	@Test(expected = IllegalArgumentException.class)
	public void whenWalkingToToInvalidLocationThenThrowException() {
		newLocation = currentLocation.getNeighbourByXAndYOffset(0, 2);
		mover = new Mover(currentLocation, new SquarePattern());

		mover.moveTo(newLocation);
	}

	@Test
	public void whenJetpackingToValidLocationThenMoveSuccessfully() {
		newLocation = currentLocation.getNeighbourByXAndYOffset(0, 2);
		mover = new Mover(currentLocation, new SparseSquarePattern());

		performMoveAndAssertions();
	}

	@Test(expected = IllegalArgumentException.class)
	public void whenJetpackingToInvalidLocationThenThrowException() {
		newLocation = currentLocation.getNeighbourByXAndYOffset(0, 1);
		mover = new Mover(currentLocation, new SparseSquarePattern());

		mover.moveTo(newLocation);
	}

	@Test
	public void whenMaglevingToValidLocationThenMoveSuccessfully() {
		newLocation = currentLocation.getNeighbourByXAndYOffset(0, 5);
		mover = new Mover(currentLocation, new BlockableLongPlusPattern());

		performMoveAndAssertions();
	}

	@Test(expected = IllegalArgumentException.class)
	public void whenMaglevingToInvalidLocationThenThrowException() {
		newLocation = currentLocation.getNeighbourByXAndYOffset(1, 5);
		mover = new Mover(currentLocation, new BlockableLongPlusPattern());

		mover.moveTo(newLocation);
	}

	private BoardCell getMiddleCellFromBoard() {
		return board.getBoardCellByXAndYCoordinates(board.getColumns() / 2, board.getRows() / 2);
	}

	private void performMoveAndAssertions() {
		assertTrue(mover.getLocation().equals(currentLocation));
		assertTrue(currentLocation.contains(mover));
		assertTrue(mover.canMoveTo(newLocation));
		assertTrue(!mover.hasMoved());

		mover.moveTo(newLocation);

		assertTrue(mover.getLocation().equals(newLocation));
		assertTrue(newLocation.contains(mover));
		assertTrue(!currentLocation.contains(mover));
		assertTrue(mover.hasMoved());
	}

	@SuppressWarnings("serial")
	private class Mover extends AbstractPlayerUnit {

		private CellPattern movementPattern;

		protected Mover(BoardCell location, CellPattern movementPattern) {
			super(location, TechnologyType.DARK_MATTER);
			this.movementPattern = movementPattern;
		}

		@Override
		public CellPattern getMovementPattern() {
			return movementPattern;
		}

		@Override
		public PlayerUnitType getPlayerUnitType() {
			return null;
		}

		@Override
		public void accept(PlayerUnitVisitor playerUnitVisitor) {
			// unused
		}

	}

}
