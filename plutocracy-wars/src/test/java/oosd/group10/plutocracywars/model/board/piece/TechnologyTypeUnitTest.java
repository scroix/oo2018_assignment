package oosd.group10.plutocracywars.model.board.piece;

import static oosd.group10.plutocracywars.model.board.piece.TechnologyType.DARK_MATTER;
import static oosd.group10.plutocracywars.model.board.piece.TechnologyType.EXPLOSIVE;
import static oosd.group10.plutocracywars.model.board.piece.TechnologyType.GRAVITY;
import static oosd.group10.plutocracywars.model.board.piece.TechnologyType.NANOTECH;
import static oosd.group10.plutocracywars.model.board.piece.TechnologyType.PLASMA;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TechnologyTypeUnitTest {
	
	@Test
	public void plasmaDefeatsExplosiveAndDarkMatter() {
		assertTrue(PLASMA.canDefeat(EXPLOSIVE));
		assertTrue(PLASMA.canDefeat(DARK_MATTER));
	}
	
	@Test
	public void explosiveDefeatsNanotechAndGravity() {
		assertTrue(EXPLOSIVE.canDefeat(NANOTECH));
		assertTrue(EXPLOSIVE.canDefeat(GRAVITY));
	}
	
	@Test
	public void nanotechDefeatsDarkMatterAndPlasma() {
		assertTrue(NANOTECH.canDefeat(DARK_MATTER));
		assertTrue(NANOTECH.canDefeat(PLASMA));
	}
	
	@Test
	public void darkMatterDefeatsGravityAndExplosive() {
		assertTrue(DARK_MATTER.canDefeat(GRAVITY));
		assertTrue(DARK_MATTER.canDefeat(EXPLOSIVE));
	}
	
	@Test
	public void gravityDefeatsPlasmaAndNanotech() {
		assertTrue(GRAVITY.canDefeat(PLASMA));
		assertTrue(GRAVITY.canDefeat(NANOTECH));
	}

}
