package oosd.group10.plutocracywars.model.board;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

public class BoardCellImplIntegrationTest {
	
	private Board board;
	private BoardCell boardCell;
	
	@Before
	public void setUp() {
		board = new BoardImpl();
	}
	
	@Test
	public void givenValidOffsetWhenGettingNeighbourThenReturnBoardCell() {
		int middleX = board.getColumns()/2;
		int middleY = board.getRows()/2;
		boardCell = board.getBoardCellByXAndYCoordinates(middleX, middleY);
		
		BoardCell neighbour = boardCell.getNeighbourByXAndYOffset(-1, -1);
		assertNotNull(neighbour);
		assertEquals(middleX-1, neighbour.getX());
		assertEquals(middleY-1, neighbour.getY());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void givenInvalidOffsetWhenGettingNeighbourThenExpectException() {
		boardCell = board.getBoardCellByXAndYCoordinates(0, 0);
		
		boardCell.getNeighbourByXAndYOffset(-1, -1);
	}

}
