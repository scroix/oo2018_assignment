package oosd.group10.plutocracywars.model.board.piece.unit;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import oosd.group10.plutocracywars.model.board.BoardCellImpl;
import oosd.group10.plutocracywars.model.board.piece.TechnologyType;

public class PlayerUnitFactoryImplUnitTest {
	private PlayerUnitFactory playerUnitFactory;
	
	@Before
	public void setUp() {
		playerUnitFactory = new PlayerUnitFactoryImpl();
	}
	
	@Test
	public void ensureFactoryHasAnImplementationForEveryPlayerUnitType() {
		PlayerUnit playerUnit = null;
		for (PlayerUnitType playerUnitType : PlayerUnitType.values()) {
			playerUnit = playerUnitFactory.createPlayerUnit(new BoardCellImpl(0, 0), playerUnitType, TechnologyType.DARK_MATTER);
			assertNotNull(playerUnit);
		}
	}
}
