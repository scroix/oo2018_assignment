package oosd.group10.plutocracywars.model.board;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

import oosd.group10.plutocracywars.model.board.BoardCellImpl;
import oosd.group10.plutocracywars.model.board.piece.BoardPiece;

public class BoardCellImplUnitTest {
	BoardPiece newOccupant = mock(BoardPiece.class);
	BoardCellImpl boardCell;

	@Before
	public void setUp() {
		boardCell = new BoardCellImpl(0, 0);
	}

	@Test
	public void givenCellIsEmptyWhenReceivingSolidPieceThenAcceptOccupant() {
		boardCell.receive(newOccupant);

		assertTrue(boardCell.contains(newOccupant));
	}

	@Test(expected = IllegalArgumentException.class)
	public void givenCellHasSolidOccupantWhenReceivingSolidPieceThenThrowException() {
		BoardPiece existingOccupant = mock(BoardPiece.class);

		boardCell.receive(existingOccupant);
		boardCell.receive(newOccupant);

		assertTrue(boardCell.contains(existingOccupant));
		assertTrue(boardCell.contains(newOccupant));
	}

}
