package oosd.group10.plutocracywars.model.board.piece.unit.pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import oosd.group10.plutocracywars.model.board.Board;
import oosd.group10.plutocracywars.model.board.BoardCell;
import oosd.group10.plutocracywars.model.board.BoardImpl;

public class SparseSquarePatternIntegrationTest {
	private Board board;
	private CellPattern cellPattern;

	@Before
	public void setUp() {
		board = new BoardImpl();
		cellPattern = new SparseSquarePattern();
	}

	@Test
	public void givenMiddleOfBoardWhenGettingCellsThenAllAreReceived() {
		BoardCell currentLocation = board.getBoardCellByXAndYCoordinates(board.getColumns() / 2, board.getRows() / 2);
		Set<BoardCell> cellsInPattern = cellPattern.getCellsInPattern(currentLocation);

		assertEquals(8, cellsInPattern.size());
		for (int xOffset = -2; xOffset <= 2; xOffset += 2) {
			for (int yOffset = -2; yOffset <= 2; yOffset += 2) {
				if (isCurrentLocation(xOffset,yOffset)) {
					continue;
				}
				assertTrue(cellsInPattern.contains(currentLocation.getNeighbourByXAndYOffset(xOffset, yOffset)));
			}
		}
	}

	@Test
	public void givenTopLeftOfBoardWhenGettingCellsThenSubsetIsReceived() {
		BoardCell currentLocation = board.getBoardCellByXAndYCoordinates(0, 0);
		Set<BoardCell> cellsInPattern = cellPattern.getCellsInPattern(currentLocation);

		assertEquals(3, cellsInPattern.size());
		assertTrue(cellsInPattern.contains(currentLocation.getNeighbourByXAndYOffset(0, 2)));
		assertTrue(cellsInPattern.contains(currentLocation.getNeighbourByXAndYOffset(2, 0)));
		assertTrue(cellsInPattern.contains(currentLocation.getNeighbourByXAndYOffset(2, 2)));
	}

	@Test
	public void givenBottomRightOfBoardWhenGettingCellsThenSubsetIsReceived() {
		BoardCell currentLocation = board.getBoardCellByXAndYCoordinates(board.getColumns() - 1, board.getRows() - 1);
		Set<BoardCell> cellsInPattern = cellPattern.getCellsInPattern(currentLocation);

		assertEquals(3, cellsInPattern.size());
		assertTrue(cellsInPattern.contains(currentLocation.getNeighbourByXAndYOffset(0, -2)));
		assertTrue(cellsInPattern.contains(currentLocation.getNeighbourByXAndYOffset(-2, 0)));
		assertTrue(cellsInPattern.contains(currentLocation.getNeighbourByXAndYOffset(-2, -2)));
	}
	
	private boolean isCurrentLocation(int xOffset, int yOffset) {
		return xOffset == 0 && yOffset == 0;
	}
}
