package oosd.group10.plutocracywars.model.board.piece.unit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import oosd.group10.plutocracywars.model.board.BoardCellImpl;
import oosd.group10.plutocracywars.model.board.piece.TechnologyType;

public class JuggernautUnitTest {

	PlayerUnit juggernaut;

	@Test
	public void givenDarkMatterTypeWhenCheckingDefeatedByThenOnlyHasOneElement() {
		juggernaut = new Juggernaut(new BoardCellImpl(0, 0), TechnologyType.DARK_MATTER);
		assertEquals(1, juggernaut.getDefeatedBy().size());
	}

	@Test
	public void givenDarkMatterTypeWhenComparingIsDefeatedByBooleanAgainstDefeatedBySetThenResultMatches() {
		juggernaut = new Juggernaut(new BoardCellImpl(0, 0), TechnologyType.DARK_MATTER);
		TechnologyType[] weaknesses = juggernaut.getDefeatedBy()
				.toArray(new TechnologyType[juggernaut.getDefeatedBy().size()]);
		TechnologyType soleWeakness = weaknesses[0];
		assertTrue(juggernaut.isDefeatedBy(soleWeakness));
	}

}
