package oosd.group10.plutocracywars.model.game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public class GameImplUnitTest {

	private static GameState firstMockGameState = mock(GameState.class);
	private static GameState secondMockGameState = mock(GameState.class);
	private static GameState thirdMockGameState = mock(GameState.class);
	private static GameState fourthMockGameState = mock(GameState.class);
	private static GameState fifthMockGameState = mock(GameState.class);
	private static GameState sixthMockGameState = mock(GameState.class);
	private static GameState seventhMockGameState = mock(GameState.class);
	private static GameState eighthMockGameState = mock(GameState.class);

	private static GameState[] gameStates = { firstMockGameState, secondMockGameState, thirdMockGameState,
			fourthMockGameState, fifthMockGameState, sixthMockGameState, seventhMockGameState, eighthMockGameState };

	private static Player mockPlayerOne = mock(Player.class);
	private static Player mockPlayerTwo = mock(Player.class);

	private static Answer<List<Player>> returnPlayerList = new Answer<List<Player>>() {
		@Override
		public List<Player> answer(InvocationOnMock invocation) throws Throwable {
			return Arrays.asList(mockPlayerOne, mockPlayerTwo);
		}
	};

	private GameImpl game;

	@BeforeClass
	public static void setUpClass() {
		for (int i = 0; i < gameStates.length; i++) {
			when(gameStates[i].toString()).thenReturn("Game state " + (i + 1));

			when(gameStates[i].getPlayers()).then(returnPlayerList);
		}
	}

	@Before
	public void setUp() {
		game = new GameImpl(0);
		game.setGameState(firstMockGameState);
		when(firstMockGameState.deepClone()).thenReturn(secondMockGameState, thirdMockGameState, fourthMockGameState,
				fifthMockGameState, sixthMockGameState, seventhMockGameState, eighthMockGameState);
	}

	@Test
	public void givenGameSetUpThenExpectedState() {
		assertEquals(0, game.numberOfResettableTurns());
		assertFalse(game.canResetTurn(mockPlayerOne));
	}

	@Test
	public void givenOneTurnHasOccurredWhenCheckingIfCanResetTurnThenReturnFalse() {
		incrementTurns(1);
		assertEquals(0, game.numberOfResettableTurns());
		assertTrue(!game.canResetTurn(mockPlayerOne));
	}

	@Test
	public void givenTwoTurnsHaveOccurredWhenCheckingIfCanResetTurnThenReturnTrue() {
		incrementTurns(2);
		assertEquals(1, game.numberOfResettableTurns());
		assertTrue(game.canResetTurn(mockPlayerOne));
	}

	@Test
	public void givenTwoTurnsHaveOccurredWhenResettingOneTurnThenGameStateIsAsExpected() {
		incrementTurns(2);
		game.resetTurn(mockPlayerOne, 1);

		assertEquals(firstMockGameState, game.getGameState());
	}

	@Test
	public void givenFourTurnsHaveOccurredWhenResettingOneTurnThenGameStateIsAsExpected() {
		incrementTurns(4);
		game.resetTurn(mockPlayerOne, 1);

		assertEquals(thirdMockGameState, game.getGameState());
	}

	@Test
	public void givenFourTurnsHaveOccurredWhenResettingTwoTurnsThenGameStateIsAsExpected() {
		incrementTurns(4);
		game.resetTurn(mockPlayerOne, 2);

		assertEquals(firstMockGameState, game.getGameState());
	}

	@Test
	public void givenThreeTurnsHaveOccurredWhenResettingOneTurnThenGameStateIsAsExpected() {
		incrementTurns(3);
		game.resetTurn(mockPlayerTwo, 1);

		assertEquals(secondMockGameState, game.getGameState());
	}

	@Test
	public void givenFiveTurnsHaveOccurredWhenResettingOneTurnThenGameStateIsAsExpected() {
		incrementTurns(5);
		game.resetTurn(mockPlayerTwo, 1);

		assertEquals(fourthMockGameState, game.getGameState());
	}

	@Test
	public void givenFiveTurnsHaveOccurredWhenResettingTwoTurnsThenGameStateIsAsExpected() {
		incrementTurns(5);
		game.resetTurn(mockPlayerTwo, 2);

		assertEquals(secondMockGameState, game.getGameState());
	}

	@Test
	public void givenPlayerOneHasAlreadyResetWhenPlayerTwoResetsOneTurnThenGameStateIsAsExpected() {
		incrementTurns(5);
		game.resetTurn(mockPlayerOne, 1);
		game.resetTurn(mockPlayerTwo, 1);

		assertEquals(secondMockGameState, game.getGameState());
	}

	@Test
	public void givenEightTurnsHaveOccurredThenFirstTimeIsRemoved() {
		incrementTurns(8);
		assertTrue(game.numberOfResettableTurns() == 3);
	}

	private void incrementTurns(int numberOfTurns) {
		for (int i = 0; i < numberOfTurns; i++) {
			game.nextTurn();
		}
	}

}
