package oosd.group10.plutocracywars.model.board;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

public class BoardImplUnitTest {
	
	private static final int COLUMNS = 12;
	private static final int ROWS = 12;
	
	private BoardImpl board;

	@Before
	public void setUp() {
		board = new BoardImpl(COLUMNS, ROWS);
	}
	
	@Test
	public void givenExpectedArgumentsWhenConstructingBoardThenStateIsInitialisedCorrectly() {
		assertTrue(board.isXCoordinateValid(0));
		assertTrue(board.isXCoordinateValid(COLUMNS-1));
		assertTrue(board.isXCoordinateValid(0));
		assertTrue(board.isXCoordinateValid(ROWS-1));
		assertFalse(board.isXCoordinateValid(-1));
		assertFalse(board.isXCoordinateValid(COLUMNS));
		assertFalse(board.isXCoordinateValid(-1));
		assertFalse(board.isXCoordinateValid(ROWS));
	}
	
	@Test
	public void givenValidArgumentsWhenGettingBoardCellThenReturnCorrectCell() {
		BoardCell cellOne = board.getBoardCellByXAndYCoordinates(0, 0);
		BoardCell cellTwo = board.getBoardCellByXAndYCoordinates(COLUMNS/2, ROWS/2);
		BoardCell cellThree = board.getBoardCellByXAndYCoordinates(COLUMNS-1, ROWS-1);
		
		assertNotNull(cellOne);
		assertNotNull(cellTwo);
		assertNotNull(cellThree);
		
		assertEquals(0, cellOne.getX());
		assertEquals(0, cellOne.getY());
		assertEquals(COLUMNS/2, cellTwo.getX());
		assertEquals(ROWS/2, cellTwo.getY());
		assertEquals(COLUMNS-1, cellThree.getX());
		assertEquals(ROWS-1, cellThree.getY());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void givenInvalidArgumentsWhenGettingBoardCellThenExpectException() {
		board.getBoardCellByXAndYCoordinates(COLUMNS, ROWS);
	}
}
