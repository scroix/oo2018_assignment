package oosd.group10.plutocracywars.model.board.piece;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import oosd.group10.plutocracywars.model.board.Board;
import oosd.group10.plutocracywars.model.board.BoardCell;
import oosd.group10.plutocracywars.model.board.BoardImpl;

public class TribbleUnitTest {

	Board board;
	BoardCell boardCell;
	Tribble tribble;

	@Before
	public void setUp() {
		board = new BoardImpl();
		boardCell = board.getBoardCellByXAndYCoordinates(board.getColumns() / 2, board.getRows() / 2);
		tribble = new Tribble(boardCell);
	}

	@Test
	public void givenTurnOccursWhenTribbleHasVacantSpaceThenChildIsSpawnedAndTribbleMoves() {
		assertTrue(boardCell.contains(tribble));

		increaseTurns(1);

		assertNotNull(tribble.getChild());
		assertFalse(boardCell.contains(tribble));
	}

	@Test
	public void givenTwoTurnsOccurWhenTribbleHasVacantSpaceThenChildIsSpawnedAndTribbleMoves() {
		increaseTurns(2);

		Tribble child = tribble.getChild();
		BoardCell childLocation = child.getLocation();
		tribble.nextTurnOccurred();
		assertNotNull(child.getChild());
		assertFalse(childLocation.contains(child));
	}

	@Test
	public void givenMultipleTriblesExistWhenFirstParentIsKilledThenAllChildrenAreKilled() {
		increaseTurns(5);
		tribble.kill();
		for (BoardCell boardCell : board.getBoardCells()) {
			assertFalse(boardCell.isOccupied());
		}
	}

	@Test
	public void givenMultipleTriblesExistWhenMidParentIsKilledThenOnlySubsequentChildrenAreKilled() {
		increaseTurns(5);
		tribble.getChild().getChild().kill();
		;

		assertNotNull(tribble.getChild());

		int countOfTribbles = 0;
		for (BoardCell boardCell : board.getBoardCells()) {
			if (boardCell.isOccupied()) {
				countOfTribbles++;
			}
		}

		assertEquals(2, countOfTribbles);
	}

	private void increaseTurns(int turns) {
		for (int i = 0; i < turns; i++) {
			tribble.nextTurnOccurred();
		}
	}
}
