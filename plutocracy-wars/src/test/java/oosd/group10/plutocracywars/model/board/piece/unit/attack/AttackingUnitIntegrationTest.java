package oosd.group10.plutocracywars.model.board.piece.unit.attack;

import static oosd.group10.plutocracywars.model.board.piece.TechnologyType.DARK_MATTER;
import static oosd.group10.plutocracywars.model.board.piece.TechnologyType.GRAVITY;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import oosd.group10.plutocracywars.model.board.Board;
import oosd.group10.plutocracywars.model.board.BoardCell;
import oosd.group10.plutocracywars.model.board.piece.TechnologyType;
import oosd.group10.plutocracywars.model.board.piece.unit.PlayerUnit;
import oosd.group10.plutocracywars.model.game.Game;
import oosd.group10.plutocracywars.model.game.GameImpl;
import oosd.group10.plutocracywars.model.game.Player;

public class AttackingUnitIntegrationTest {
	
	private static final Logger LOG = LogManager.getLogger(AttackingUnitIntegrationTest.class);

	private Game game;

	private Board board;

	private AbstractAttackingUnit attacker;
	private BoardCell attackerLocation;
	private TechnologyType attackerTechnologyType;

	private PlayerUnit defender;
	private BoardCell defenderLocation;
	private TechnologyType defenderTechnologyType;

	private Player playerOne;
	private Player playerTwo;

	@Before
	public void setUp() {
		game = new GameImpl();
		board = game.getBoard();
		playerOne = game.getPlayers().get(0);
		playerTwo = game.getPlayers().get(1);
	}

	@After
	public void tearDown() {
		attacker = null;
		attackerLocation = null;
		attackerTechnologyType = null;

		defender = null;
		defenderLocation = null;
		defenderTechnologyType = null;
	}

	@Test
	public void givenRiflemanWhenAttackingUnitAtValidLocationAndValidTechnologyThenDefenderIsKilled() {
		attackerLocation = getMiddleCellFromBoard();
		attackerTechnologyType = DARK_MATTER;
		attacker = new Rifleman(attackerLocation, attackerTechnologyType);

		defenderLocation = attackerLocation.getNeighbourByXAndYOffset(0, 4);
		defenderTechnologyType = GRAVITY;
		defender = new Rifleman(defenderLocation, defenderTechnologyType);

		attackSingleDefenderAndAssert();
	}

	@Test
	public void givenBeserkerWhenAttackingUnitAtValidLocationAndValidTechnologyThenDefenderIsKilled() {
		attackerLocation = getMiddleCellFromBoard();
		attackerTechnologyType = DARK_MATTER;
		attacker = new Beserker(attackerLocation, attackerTechnologyType);

		defenderLocation = attackerLocation.getNeighbourByXAndYOffset(1, 0);
		defenderTechnologyType = GRAVITY;
		defender = new Rifleman(defenderLocation, defenderTechnologyType);

		attackSingleDefenderAndAssert();
	}

	@Test
	public void givenBeserkerWhenAttackingMultipleUnitsAtValidLocationAndValidTechnologyThenAllDefendersAreKilled() {
		attackerLocation = getMiddleCellFromBoard();
		attackerTechnologyType = DARK_MATTER;
		attacker = new Beserker(attackerLocation, attackerTechnologyType);
		playerOne.addUnit(attacker);

		defenderTechnologyType = GRAVITY;

		defenderLocation = attackerLocation.getNeighbourByXAndYOffset(-1, 0);
		BoardCell defender2Location = attackerLocation.getNeighbourByXAndYOffset(0, 1);
		BoardCell defender3Location = attackerLocation.getNeighbourByXAndYOffset(1, 0);
		BoardCell defender4Location = attackerLocation.getNeighbourByXAndYOffset(0, -1);

		defender = new Rifleman(defenderLocation, defenderTechnologyType);
		PlayerUnit defender2 = new Rifleman(defender2Location, defenderTechnologyType);
		PlayerUnit defender3 = new Rifleman(defender3Location, defenderTechnologyType);
		PlayerUnit defender4 = new Rifleman(defender4Location, defenderTechnologyType);

		playerTwo.addUnit(defender);
		playerTwo.addUnit(defender2);
		playerTwo.addUnit(defender3);
		playerTwo.addUnit(defender4);

		assertPlayerOneHasNUnits(1);
		assertPlayerTwoHasNUnits(4);

		assertAttackerCanAttack();
		
		LOG.debug("Current player is {}", game.getCurrentPlayer().getName());

		attacker.attack(defenderLocation);

		assertAttackerHasAttackedAndSurvived();

		assertPlayerTwoHasNUnits(0);

		assertDefenderIsKilled(defender);
		assertDefenderIsKilled(defender2);
		assertDefenderIsKilled(defender3);
		assertDefenderIsKilled(defender4);
	}

	@Test
	public void givenChargerWhenAttackingUnitAtValidLocationAndValidTechnologyThenDefenderIsKilled() {
		attackerLocation = getMiddleCellFromBoard();
		attackerTechnologyType = DARK_MATTER;

		defenderLocation = attackerLocation.getNeighbourByXAndYOffset(0, 4);
		defenderTechnologyType = GRAVITY;
	}

	private BoardCell getMiddleCellFromBoard() {
		return board.getBoardCellByXAndYCoordinates(board.getColumns() / 2, board.getRows() / 2);
	}

	private void attackSingleDefenderAndAssert() {
		playerOne.addUnit(attacker);
		playerTwo.addUnit(defender);

		assertPlayerOneHasNUnits(1);
		assertPlayerTwoHasNUnits(1);

		assertAttackerCanAttack();

		attacker.attack(defenderLocation);

		assertAttackerHasAttackedAndSurvived();

		assertPlayerTwoHasNUnits(0);

		assertDefenderIsKilled();
	}

	private void assertAttackerCanAttack() {
		assertTrue(attacker.canAttack(defenderLocation));
		assertTrue(!attacker.hasAttacked());
	}

	private void assertAttackerHasAttackedAndSurvived() {
		assertTrue(attacker.hasAttacked());
		assertTrue(playerOne.getUnits().size() == 1);
	}

	private void assertDefenderIsKilled() {
		assertDefenderIsKilled(defender);
	}

	private void assertDefenderIsKilled(PlayerUnit defender) {
		assertNull(defender.getOwner());
		assertNull(defender.getLocation());
	}

	private void assertPlayerOneHasNUnits(int numberOfUnits) {
		assertTrue(playerOne.getUnits().size() == numberOfUnits);
	}

	private void assertPlayerTwoHasNUnits(int numberOfUnits) {
		assertTrue(playerTwo.getUnits().size() == numberOfUnits);
	}

}
