package oosd.group10.plutocracywars.model.board.piece.unit.pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import oosd.group10.plutocracywars.model.board.Board;
import oosd.group10.plutocracywars.model.board.BoardCell;
import oosd.group10.plutocracywars.model.board.BoardImpl;
import oosd.group10.plutocracywars.model.board.piece.BoardPiece;
import oosd.group10.plutocracywars.model.board.piece.AbstractBoardPiece;
import oosd.group10.plutocracywars.model.board.piece.TechnologyType;

public class BlockableLongPlusPatternIntegrationTest {
	private Board board;
	private CellPattern cellPattern;

	@Before
	public void setUp() {
		board = new BoardImpl();
		cellPattern = new BlockableLongPlusPattern();
	}

	@Test
	public void givenMiddleOfBoardWhenGettingCellsThenAllAreReceived() {
		BoardCell currentLocation = board.getBoardCellByXAndYCoordinates(board.getColumns() / 2, board.getRows() / 2);
		Set<BoardCell> cellsInPattern = cellPattern.getCellsInPattern(currentLocation);

		assertEquals(22, cellsInPattern.size());
		for (int xOffset = -6; xOffset < 6; xOffset++) {
			if (isCurrentLocation(xOffset, 0)) {
				continue;
			}
			assertTrue(cellsInPattern.contains(currentLocation.getNeighbourByXAndYOffset(xOffset, 0)));
		}
		for (int yOffset = -6; yOffset < 6; yOffset++) {
			if (isCurrentLocation(0, yOffset)) {
				continue;
			}
			assertTrue(cellsInPattern.contains(currentLocation.getNeighbourByXAndYOffset(0, yOffset)));
		}
	}

	@Test
	public void givenTopLeftOfBoardWhenGettingCellsThenSubsetIsReceived() {
		BoardCell currentLocation = board.getBoardCellByXAndYCoordinates(0, 0);
		Set<BoardCell> cellsInPattern = cellPattern.getCellsInPattern(currentLocation);

		assertEquals(22, cellsInPattern.size());
		for (int xOffset = 1; xOffset < 12; xOffset++) {
			assertTrue(cellsInPattern.contains(currentLocation.getNeighbourByXAndYOffset(xOffset, 0)));
		}
		for (int yOffset = 1; yOffset < 12; yOffset++) {
			assertTrue(cellsInPattern.contains(currentLocation.getNeighbourByXAndYOffset(0, yOffset)));
		}
	}

	@Test
	public void givenBottomRightOfBoardWhenGettingCellsThenSubsetIsReceived() {
		BoardCell currentLocation = board.getBoardCellByXAndYCoordinates(board.getColumns() - 1, board.getRows() - 1);
		Set<BoardCell> cellsInPattern = cellPattern.getCellsInPattern(currentLocation);

		assertEquals(22, cellsInPattern.size());
		for (int xOffset = -11; xOffset < 0; xOffset++) {
			assertTrue(cellsInPattern.contains(currentLocation.getNeighbourByXAndYOffset(xOffset, 0)));
		}
		for (int yOffset = -11; yOffset < 0; yOffset++) {
			assertTrue(cellsInPattern.contains(currentLocation.getNeighbourByXAndYOffset(0, yOffset)));
		}
	}

	@Test
	@SuppressWarnings("unused")
	public void givenMiddleOfBoardWithAbitraryObstaclesWhenGettingCellsThenSubsetIsReceived() {
		BoardCell currentLocation = board.getBoardCellByXAndYCoordinates(board.getColumns() / 2, board.getRows() / 2);
		BoardPiece topBlocker = new MockBoardPiece(currentLocation.getNeighbourByXAndYOffset(0, -3));
		BoardPiece rightBlocker = new MockBoardPiece(currentLocation.getNeighbourByXAndYOffset(4, 0));
		BoardPiece bottomBlocker = new MockBoardPiece(currentLocation.getNeighbourByXAndYOffset(0, 1));
		Set<BoardCell> cellsInPattern = cellPattern.getCellsInPattern(currentLocation);

		assertEquals(11, cellsInPattern.size());
		for (int xOffset = -6; xOffset < 4; xOffset++) {
			if (isCurrentLocation(xOffset, 0)) {
				continue;
			}
			assertTrue(cellsInPattern.contains(currentLocation.getNeighbourByXAndYOffset(xOffset, 0)));
		}
		for (int yOffset = -2; yOffset < 0; yOffset++) {
			if (isCurrentLocation(0, yOffset)) {
				continue;
			}
			assertTrue(cellsInPattern.contains(currentLocation.getNeighbourByXAndYOffset(0, yOffset)));
		}
	}

	private boolean isCurrentLocation(int xOffset, int yOffset) {
		return xOffset == 0 && yOffset == 0;
	}

	private class MockBoardPiece extends AbstractBoardPiece {
		/**
		 * 
		 */
		private static final long serialVersionUID = 7687648952865414496L;

		public MockBoardPiece(BoardCell location) {
			super(location, TechnologyType.DARK_MATTER);
		}
	}
}
