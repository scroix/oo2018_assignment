# oo2018_assignment

Assignment for ISYS1084 (Object Oriented Software Design) 2018 Semester 1

### Infinite Loop

- Christopher Lemarshall 3482127

- Julien de-Sainte-Croix 3369242

- Thomas Maw                3685406

- Venkata N Sistla        3560634

### Quick Start

`mvn build`
`run-with-dbc.bat` *from a Windows machine*

`java -jar target/plutocracy-wars-0.0.1-SNAPSHOT.jar` *otherwise*

### SOLID, GRASP and DbC

See BoardPieces & PlayerUnits diagram on Page 12 of InfiniteLoop_a1.pdf for best example of SOLID and GRASP.
The primary Cofoja example is between the `oosd.group10.plutocracywars.model.board.boardimpl.java | .board.java` classes. It's also discussed in Page 6 of the aforementioned PDF.

## Functional Requirements

#### High Priority

- **Capture** function.

#### Mandatory

- An **undo moves** option.

- Add an **additional combat capability** to each piece.

#### Additional

- An option to **create different sized boards and varying number of pieces**

- An option to **introduce obstacles** onto the game board.

## Non-Functional Requirements

- The use of, and addition of class diagrams supporting the use of six of the following patterns, two from each category.
- **Creational**
  - Abstract Factory
  - Prototype
- **Structural**
  - Composite
  - Decorator
- **Behavioural**
  - Visitor
  - Observer
  - Chain of Responsibility
  - Command
  - Bridge
  - Persistent Data Structure
